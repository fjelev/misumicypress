// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
const authUser = require('../fixtures/auth-user.json');
const {
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

import Login from "../integration/boc/pom/login"
import {
    addMatchImageSnapshotCommand
} from 'cypress-image-snapshot/command'

addMatchImageSnapshotCommand({
    failureTreshold: 3.00,
    failureTresholdType: 'percent',
    customDiffConfig: {
        treshold: 0.0
    },
    capture: 'viewport'
})

Cypress.Commands.add("setResolution", (size) => {
    if (Cypress._.isArray(size)) {
        cy.viewport(size[0], size[1])
    } else {
        cy.viewport(size)
    }
})

const login = new Login()

// -- This is a parent command --

Cypress.Commands.add("changeLanguage", (language) => {
    cy.get(`.lc-lang--${language}`).click({
        force: true
    })
});

Cypress.Commands.add("waitForRequest", (requestLink, requestName) => {
    cy.intercept('GET', `${requestLink}`).as(`${requestName}`)
    cy.wait(`@${requestName}`)
    cy.get(`@${requestName}`).then(xhr => {
        console.log(xhr.response.body)
    })
    log.end();
});

Cypress.Commands.add("login", (username = 'abc', password = 'abc') => {

    const loginFunc = (username, password, name, url) => {
        const log = Cypress.log({
            name: "login",
            displayName: `LOGIN ${url}`,
            message: [`🔐 Authenticating | ${username}`],
            autoEnd: false
        })

        cy.intercept('GET', '**userInfo**').as('loginUser')

        login.acceptCookies().click()
        const loginMenuButton = cy.get('[data-login="loginButton"]');
        loginMenuButton.click()
        login.username().type(username)
        login.password().type(password)
        login.signInButton().click()
        cy.wait("@loginUser")
        cy.get('@loginUser').then(xhr => {
            console.log(xhr.response.body.userName)
        })
        cy.contains(name)
        log.end();
    }

    cy.url().then(url => {
        if (url.includes('stg0')) {
            cy.fixture('stg0-env').then(env => {
                const {
                    username,
                    password,
                    name
                } = env
                loginFunc(username, password, name, url)
            })
        } else {
            cy.fixture('live-env').then(env => {
                const {
                    username,
                    password,
                    name
                } = env
                loginFunc(username, password, name, url)
            })
        }
    })

})




Cypress.Commands.add("searchPart", (partName) => {
    cy.get('#keyword_input').type(partName)
    cy.get('.l-header__keywordBox--productCode > dd > ul > li > a').contains(partName);
    cy.get('.l-header__keywordBox--productCode > dd > ul > li > a').first().click()
});

Cypress.Commands.add("addToCart", (partName, numberItems) => {
    cy.get('#keyword_input').type(partName)
    cy.get('div.l-header__searchWrap > div > div > div > dl > dd > ul > li > a').first().click()

    if (cy.get('.m-h3')) {
        cy.get('.m-inputText--right').type(`${numberItems}`)
        cy.intercept('POST', '**api/v1/cart/add**').as('addToCart')
        cy.get('.m-btn--cartin').click()
        cy.wait("@addToCart")
        cy.get('.m-btn--close').click()
    }
});


// https://api.uk.misumi-ec.com/api/v1/cart/add?lang=ENG&suppressResponseCode=true&applicationId=3613ed15-0f92-4dc2-8ebc-5fb1e2e21816&_=1615806150835&sessionId=sdlctCfeLyzJtZ1uwXDr1A2hbR5YYqGldArmiUq7UbiuYPmRgBS7IYLqrKi81w6Zfsk0t60Hsx8XcrREM3fHySgQhf0PE4vrSzaE9FuDactK9zrZPhwsWHyaISE4ILez-ednB9M3E_Wuu-tGBN3RUlQGqPypgGA_SrLbKWUTbKVVC8slVPGJgp_UncZAHheMb91jaKj6oIE-t8exMLszs8SIRYT2OSXliM4TsGwpj8GLlJ4eGiD711xI5nP-gNoJFiJJJljdczaYlC3ZPcF_Bc57eGwQ-x-9K6xbhDaUNPsuaiSdutwfMgVFbLZC1MWLeDanYZzKDpYJcHrSODThNvzLUxYjDUJflP4pakUanNqTCEXN4ffrITfnbIqdUNk_57kapM4qXrog1wewr3uBE8WQ5n7LZ7uw_1U5WSiyY9f5HKrM-px3pVG56SGr2dgC

Cypress.Commands.add("deleteAllCart", () => {
    cy.get('.lc-count').invoke('text').then(x => {
        cy.log(parseFloat(x))
        const items = parseFloat(x)
        cy.log(items)
        if (items >= 1) {
            cy.log('less than 1')
            cy.get('.lc-cart').click()
            cy.get('.button--delete').first().click()
            cy.get('#button_delete_cart').click()
        }
    })
    // if(cy.get('.lc-count').invoke('text').then(parseFloat).should('be.gt', 0)) {
    //         cy.get('.lc-cart').click()
    //         cy.get('.button--delete').first().click()
    //         cy.get('#button_delete_cart').click()
    //     }
});

const arrUsers = [{
        user: "corporatetest1@blubito.ta",
        available: true
    },
    {
        user: "corporatetest2@blubito.ta",
        available: true
    },
    {
        user: "corporatetest3@blubito.ta",
        available: true
    },
    {
        user: "corporatetest4@blubito.ta",
        available: true
    },
    {
        user: "corporatetest5@blubito.ta",
        available: true
    },
    {
        user: "corporatetest6@blubito.ta",
        available: true
    },
    {
        user: "corporatetest7@blubito.ta",
        available: true
    },
];

const arrUsersStg = [{
        user: "stgctest1@blubito.ta",
        available: true
    },
    {
        user: "stgctest2@blubito.ta",
        available: true
    },
    {
        user: "stgctest3@blubito.ta",
        available: true
    },
    {
        user: "stgctest4@blubito.ta",
        available: true
    },
    {
        user: "stgctest5@blubito.ta",
        available: true
    },
    {
        user: "stgctest6@blubito.ta",
        available: true
    },
    {
        user: "stgctest7@blubito.ta",
        available: true
    },
];
let switchArr = undefined;

Cypress.Commands.add("cartLogin", (username, password) => {
    const log = Cypress.log({
        name: "login",
        message: [`🔐 Authenticating | ${username}`],
        autoEnd: false
    })

    cy.intercept('GET', '**userInfo**').as('loginUser')

    login.acceptCookies().click()
    const loginMenuButton = cy.get('[data-login="loginButton"]');
    loginMenuButton.click()
    login.username().type(username)
    login.password().type(password)
    login.signInButton().click()
    cy.wait("@loginUser")
    log.end();
})

export const getFirstAvailableUser = () => {
    // if (environment.username === "WOS@3533268") {
    //   return undefined;
    // }
    const getURLTxt = url_uk;
    let availableUSER = undefined;

    if (getURLTxt.includes("stg0")) {
        switchArr = [...arrUsersStg];
    } else {
        switchArr = [...arrUsers];
    }

    availableUSER = switchArr.filter((user) => user.available === true)[0];
    while (!availableUSER) {
        cy.wait(7000);
        availableUSER = switchArr.filter(
            (user) => user.available === true
        )[0];
    }

    const indexUserUK = switchArr.findIndex(
        (username) => username.user === availableUSER.user
    );
    switchArr[indexUserUK].available = false;
    // console.log('UK user is selected: ', switchArr[indexUserUK]);
    switchArr.forEach(user => {
        cy.log(user.user, user.available)
    })
    return availableUSER.user;
};

export const releaseUser = (usedUser) => {
    if (!usedUser) {
        return;
    }
    const indexUserUK = switchArr.findIndex(
        (username) => username.user === usedUser
    );
    switchArr[indexUserUK].available = true;
    switchArr.forEach(user => {
        cy.log(user.user, user.available)
    })
};

//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })