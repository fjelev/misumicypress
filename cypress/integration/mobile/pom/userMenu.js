class UserMenu {
    userPanel() {
        return cy.get('[data-header-func="user"]');
    }

    myPageUserMenu() {
        return cy.get('[data-common-userlink="mypagetop"]')
    }

    myPageSideMenu() {
        return cy.get('[data-common-userlink="mypagetop"]')
    }

    myCadDownloads() {
        return cy.get('[data-common-userlink="cadhistory"]')
    }

    myComponents() {
        return cy.get('[data-common-userlink="partslist"]')
    }

    changeUserData() {
        return cy.get('[data-common-userlink="editprofile"]')
    }

    changePassword() {
        return cy.get('[data-common-userlink="passchange"]')
    }


}

export default UserMenu;