const authUser = require('../../../fixtures/auth-user.json');

class Login {
    signIn() {
        const { username, password } = authUser;
        this.acceptCookies().click()
        const loginMenuButton = cy.get('[data-login="loginButton"]');
        loginMenuButton.click()
        this.username().type(username)
        this.password().type(password)
        this.signInButton().click()
        return this
    }

    mobileSignIn() {
        const { username, password } = authUser;
        this.acceptCookies().click()
        cy.get('[data-chenge-mobile]').click()
        cy.get('[data-header-func="login"]').click()
        this.username().type(username)
        this.password().type(password)
        this.signInButton().click()
        return this
    }

    acceptCookies() {
        return cy.get('.l-cookieNotification__accept');
    }

    username() {
        return cy.get('[data-login="userid"]')
    }    
    
    password() {
        return cy.get('[data-login="password"]')
    }

    signInButton(){
        return cy.get('[data-login="submit"]')
    }
}

export default Login;