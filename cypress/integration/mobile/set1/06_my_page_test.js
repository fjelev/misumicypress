import Login from '../pom/login';
import MyPage from '../pom/myPage';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const myPage = new MyPage();
const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('My Page - Login Menu UK', () => {
  beforeEach(() => {
    cy.viewport('iphone-x')
  })
  
  it('My Page - Login Menu UK', () => {
    cy.visit(url_uk)
    const { username, password } = authUser;

    login.acceptCookies().click()
    cy.wait(3000)
    cy.get('[data-chenge-mobile]').click()
    cy.wait(1000)
    cy.get('[data-header-func="login"]').click()
    login.username().type(username)
    login.password().type(password)
    login.signInButton().click()
    cy.wait(10000)
    cy.get('[data-header-func="login"]').click()
    userMenu.myPageUserMenu().click()

    myPage.pageTitle().contains('My Page')
    myPage.pageTopic().contains('MISUMI Top Page')
  })

  it('My Page - Login Menu DE', () => {
    cy.visit(url_de)
    const { username, password } = authUser;

    login.acceptCookies().click()
    cy.wait(3000)
    cy.get('[data-chenge-mobile]').click()
    cy.wait(1000)
    cy.get('[data-header-func="login"]').click()
    login.username().type(username)
    login.password().type(password)
    login.signInButton().click()
    cy.wait(10000)
    cy.get('[data-header-func="login"]').click()
    userMenu.myPageUserMenu().click()

    myPage.pageTitle().contains('Meine Seite')
    myPage.pageTopic().contains('MISUMI Startseite')
  })

  it('My Page - Login Menu IT', () => {
    cy.visit(url_it)
    const { username, password } = authUser;

    login.acceptCookies().click()
    cy.wait(3000)
    cy.get('[data-chenge-mobile]').click()
    cy.wait(1000)
    cy.get('[data-header-func="login"]').click()
    login.username().type(username)
    login.password().type(password)
    login.signInButton().click()
    cy.wait(10000)
    cy.get('[data-header-func="login"]').click()
    userMenu.myPageUserMenu().click()

    myPage.pageTitle().contains('La mia pagina')
    myPage.pageTopic().contains('Pagina principale MISUMI')
  })

  it('My Page - Login Menu FR', () => {
    cy.visit(url_fr)
    const { username, password } = authUser;

    login.acceptCookies().click()
    cy.wait(3000)
    cy.get('[data-chenge-mobile]').click()
    cy.wait(1000)
    cy.get('[data-header-func="login"]').click()
    login.username().type(username)
    login.password().type(password)
    login.signInButton().click()
    cy.wait(10000)
    cy.get('[data-header-func="login"]').click()
    userMenu.myPageUserMenu().click()

    myPage.pageTitle().contains('Ma page')
    myPage.pageTopic().contains('Haut de la page MISUMI')
  })
});
