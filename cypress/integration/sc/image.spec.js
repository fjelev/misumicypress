// describe('Visual Regression', () => {


//     it('Firtst VR', () => {
//         //load website
//         cy.visit("http://example.com/")    
//         // compare
//         cy.matchImageSnapshot()
//     })   
     
// })

describe('Firtst VR', () => {
    const pages = ["http://example.com/"]
    const sizes = ["iphone-6", "ipad-2", [1200, 800]]
    sizes.forEach(size => {
        pages.forEach(page => {
            it(`should match ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                cy.matchImageSnapshot()
            })
        })
    })
    
    it("Only one element", () => {
        cy.visit("http://example/")
        cy.get("h1").matchImageSnapshot()
    })
})