import Login from '../boc/pom/login'

const authUser = require('../../fixtures/auth-user.json');
const login = new Login();
const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Homepage Header Screenshot Checks', () => {
    const pages = [url_uk, url_de, url_it, url_fr]
    const sizes = [[1920, 1080]]
    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Header image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                cy.wait(3000)
                login.acceptCookies().click()
                cy.get('body > div.l-wrapper > div:nth-child(1)').matchImageSnapshot()
            })
        })
    })
})