import Login from '../boc/pom/login'

const authUser = require('../../fixtures/auth-user.json');
const login = new Login();
const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Homepage Footer Screenshot Checks', () => {
    const pages = [url_uk, url_de, url_it, url_fr]
    const sizes = [[1920, 1080]]
    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Footer image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                cy.wait(3000)
                login.acceptCookies().click()
                cy.get(".l-footer").matchImageSnapshot()
            })
        })
    })
})