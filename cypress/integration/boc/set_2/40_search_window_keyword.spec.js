import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('40 Search Window Keyword', () => {

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: search results of LINEAR are displayed in Category of the page (MISUMI Top Page> Search results).
  */
  it('Search Window Keyword UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('Linear')
    cy.get('#keyword_go').click()
    cy.contains('Linear Motion')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: search results of LINEAR are displayed in Category of the page (MISUMI Top Page> Search results).
  */
  it('Search Window Keyword DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('Linearbewegung')
    cy.get('#keyword_go').click()
    cy.contains('Linearbewegung')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: search results of LINEAR are displayed in Category of the page (MISUMI Top Page> Search results).
  */
  it('Search Window Keyword IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('Movimentazione')
    cy.get('#keyword_go').click()
    cy.contains('Movimentazione lineare')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: search results of LINEAR are displayed in Category of the page (MISUMI Top Page> Search results).
  */
  it('Search Window Keyword FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('linéaire')
    cy.get('#keyword_go').click()
    cy.contains('Arbre linéaire')
  })

});