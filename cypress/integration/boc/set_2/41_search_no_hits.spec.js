import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('41 Search Result No Hits', () => {

  /*
    Description: eCatalog Top Page - Enter @@@ in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: Message for no results is displayed in the page (MISUMI Top Page> Search results). (The searched keyword returned no results.)
  */
  it('Search Result No Hits UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('@@@')
    cy.get('#keyword_go').click()
    cy.contains('The searched keyword returned no results.')
  })

  /*
    Description: eCatalog Top Page - Enter @@@ in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: Message for no results is displayed in the page (MISUMI Top Page> Search results). (The searched keyword returned no results.)
  */
  it('Search Result No Hits DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('@@@')
    cy.get('#keyword_go').click()
    cy.contains('Die Suche nach dem Stichwort ergab keine Ergebnisse.')
  })

  /*
    Description: eCatalog Top Page - Enter @@@ in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: Message for no results is displayed in the page (MISUMI Top Page> Search results). (The searched keyword returned no results.)
  */
  it('Search Result No Hits IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('@@@')
    cy.get('#keyword_go').click()
    cy.contains('La parola chiave ricercata non ha restituito alcun risultato.')
  })
  
  /*
    Description: eCatalog Top Page - Enter @@@ in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: Message for no results is displayed in the page (MISUMI Top Page> Search results). (The searched keyword returned no results.)
  */
  it('Search Result No Hits FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    
    cy.get('#keyword_input').type('@@@')
    cy.get('#keyword_go').click()
    cy.contains('Le mot-clé recherché n\'a donné aucun résultat.')
  })
});