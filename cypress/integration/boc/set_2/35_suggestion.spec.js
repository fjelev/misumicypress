import {
    beforeEach
} from 'mocha';
import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;
describe('35 Suggestion', () => {
    const partName = 'BH6'
    /*
        Description: eCatalog top page - Enter BH6 in the search field at the top of the page.
        Test Steps:
            1. Suggestion for product without information posted is displayed whose part number is inputted. (BH6 [SUNFLAG] *No Product Page (Available to Order))
    */
    it('Suggestion UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type(partName)
        cy.contains('BH6 [SUNFLAG] ※No Product Page (Available to Order)');
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type(partName)
        cy.wait(1000)
        cy.contains('BH6 [SUNFLAG] ※Seite Kein Produkt (bestellbar)');
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type(partName)
        cy.contains('BH6 [SUNFLAG] ※No Pagina dei prodotti (disponibili su ordinazione)');
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type(partName)
        cy.contains('BH6 [SUNFLAG] ※Aucune page de produit (disponible en commande)');
    })

    /*
        Description: Click the link [BH6 [SUNFLAG] *No Product Page (Available to Order)] which is suggested.
        Test Steps:
            1. Modal of quotation and order for product without information posted is displayed.  (No Product Page (Available to Order))
    */
    it('Suggestion UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type(partName)
        cy.contains('BH6 [SUNFLAG] ※No Product Page (Available to Order)').click()
        cy.get('.m-btn--cartin')
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type(partName)
        cy.wait(1000)
        cy.contains('BH6 [SUNFLAG] ※Seite Kein Produkt (bestellbar)').click()
        cy.get('.m-btn--cartin')
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type(partName)
        cy.contains('BH6 [SUNFLAG] ※No Pagina dei prodotti (disponibili su ordinazione)').click()
        cy.get('.m-btn--cartin')
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type(partName)
        cy.contains('BH6 [SUNFLAG] ※Aucune page de produit (disponible en commande)').click()
        cy.get('.m-btn--cartin')
    })

})