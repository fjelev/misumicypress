import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('30 Breadcrumbs', () => {
/*
  Description: Product details page [Straight] Confirm the breadcrumbs at the top of the page.
  Test Steps:
      1. The notation of breadcrumbs is shown as below. MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight
*/ 
  it('Breadcrumbs UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Automation Components').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linear Shafts').click()
    cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
    cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
    cy.wait("@loadCategory")
    cy.contains('MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight')
  })

/*
  Description: Product details page [Straight] Confirm the breadcrumbs at the top of the page.
  Test Steps:
      1. The notation of breadcrumbs is shown as below. MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight
*/ 
  it('Breadcrumbs DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Mechanische Komponenten').click({force: true})
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linearwellen').click()
    cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
    cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
    cy.wait("@loadCategory")

    cy.contains('MISUMI Startseite> Mechanische Komponenten> Linearbewegung> Linearwellen> Linearwellen / gerade / Bearbeitung wählbar')
  })

/*
  Description: Product details page [Straight] Confirm the breadcrumbs at the top of the page.
  Test Steps:
      1. The notation of breadcrumbs is shown as below. MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight
*/ 
  it('Breadcrumbs IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('[href="/vona2/mech/"]').click({force: true})
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Alberi lineari').click()
    cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
    cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
    cy.wait("@loadCategory")

    cy.contains('Pagina principale MISUMI> Componenti meccanici> Movimentazione lineare> Alberi lineari> Dritti')
  })

/*
  Description: Product details page [Straight] Confirm the breadcrumbs at the top of the page.
  Test Steps:
      1. The notation of breadcrumbs is shown as below. MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight
*/ 
  it('Breadcrumbs IT', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('[href="/vona2/mech/"]').click({force: true})
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Arbres linéaires').click()
    cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
    cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
    cy.wait("@loadCategory")

    cy.contains('Haut de la page MISUMI> Composants mécaniques> Mouvement linéaire> Arbres linéaires> Droit')
  })

});