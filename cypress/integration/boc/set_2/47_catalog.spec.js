import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('47 Digital Book', () => {

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [Catalog] button at the top right of the page.
    Test Steps:
        1. Digital Book is displayed.
  */
  it('Digital Book UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

  it('Digital Book DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

  it('Digital Book IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

  it('Digital Book FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

});