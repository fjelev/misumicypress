import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('49 CAD Download', () => {

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [CAD Download] on the right side of the page.
    Test Steps:
        1. *When Terms of use of CAD data is displayed, click [Agree].
        2. CAD data selection page is displayed.
  */
  it('CAD Download UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#cad_dl_button').click()
    cy.get('#CAD_agree').click()
    cy.contains('Generate')
  })

  /*
    Description: Click the [2d_SFJ.zip] link from 2D CAD data with fixed dim. of CAD data selection page.
    Test Steps:
        1. The file 2d_SFJ.zip is downloaded. 
  */
  it('CAD Download DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#cad_dl_button').click()
    cy.get('#CAD_agree').click()
    // cy.contains('Generieren')
  })

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [CAD Download] on the right side of the page.
    Test Steps:
        1. *When Terms of use of CAD data is displayed, click [Cancel].
  */
  it('CAD Download IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#cad_dl_button').click()
    cy.get('#CAD_agree').click()
    // cy.contains('Genera')
  })

  /*
    Description: Select File Format : DWF and Version : V5.5,ASCII
    Test Steps:
        1. click [Generate] of 2D/3D CAD Download of CAD data select page.
  */
  it('CAD Download FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#cad_dl_button').click()
    cy.get('#CAD_agree').click()
    // cy.contains('Générer')
  })

});