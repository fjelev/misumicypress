
 import Login from '../pom/login';
 import MyPage from '../pom/myPage';
 import SideMenu from '../pom/sideMenu';
 import UserMenu from '../pom/userMenu';
 
 const authUser = require('../../../fixtures/auth-user.json');
 const login = new Login();
 const userMenu = new UserMenu();
 const sideMenu = new SideMenu();
 const myPage = new MyPage();
 const {
     username,
     password,
     url_uk,
     url_de,
     url_fr,
     url_it
 } = authUser;
 
 describe('65 Cart Transfer to My Components ', () => {
     
   /*
     Description: Select only SFJ3-10 on the cart page and click the [Go to my component list] button on the top of the page.

 
     Test Steps:
         1. Modal which confirms a product moves to My components is displayed.
            (Go to my component list)
         2. Click the [Move] button in the modal which confirms that a product is moved to My components.
         Modal which shows a product was moved to My components is displayed.
            (Go to my component list)
        3. Click the [Close] button in the modal which shows that a product was moved to My components.
            The display of SFJ3-10 disappeared which moved from cart to My components, and the registered number of items decreases by one.
        4. Display the main folder page of My components and confirm that moved SFJ3-10 was added from the cart. *Display method of main folder page of My components is optional.
            SFJ3-10 is added in main folder of My components.
 
   */
     it('Cart Transfer to My Components UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
 
         cy.searchPart('SFJ3-100')
         cy.contains('SFJ3-100')
         
     })

     it('Cart Transfer to My Components DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('SFJ3-100')
        cy.contains('SFJ3-100')
        
    })

    it('Cart Transfer to My Components IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('SFJ3-100')
        cy.contains('SFJ3-100')
        
    })

    it('Cart Transfer to My Components FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('SFJ3-100')
        cy.contains('SFJ3-100')
        
    })
 
 });