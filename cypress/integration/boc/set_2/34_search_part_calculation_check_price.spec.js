import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('34 Price Check', () => {

    const partName = 'LHQRW10'
    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains('Linear Bushings with Grease Nipples')
        cy.get('#ProductCode_nocatalog').type(partName).type('{enter}')
        cy.get('.m-btn--checkPrice')
        cy.get('.m-inputText--right').type('3')
        cy.get('.m-btn--checkPrice').click()
        cy.contains('49.08')
    })

    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('LHQRW10')
        cy.url().should('include', 'LHQRW10')
        cy.wait(3000)
        cy.changeLanguage('de')
        cy.contains('Linearkugellager mit Schmiernippel')
        cy.get('#ProductCode_nocatalog').type(partName).type('{enter}')
        cy.get('.m-btn--checkPrice')
        cy.get('.m-inputText--right').type('3')
        cy.get('.m-btn--checkPrice').click()
        cy.contains('49.08')
    })

    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('LHQRW10')
        cy.url().should('include', 'LHQRW10')
        cy.wait(3000)
        cy.changeLanguage('it')
        cy.contains('Boccole lineari con ingrassatore')
        cy.get('#ProductCode_nocatalog').type(partName).type('{enter}')
        cy.get('.m-btn--checkPrice')
        cy.get('.m-inputText--right').type('3')
        cy.get('.m-btn--checkPrice').click()
        cy.contains('49.08')
    })

    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('LHQRW10')
        cy.url().should('include', 'LHQRW10')
        cy.wait(3000)
        cy.changeLanguage('fr')
        cy.contains('Manchons linéaires à graisseur')
        cy.get('#ProductCode_nocatalog').type(partName).type('{enter}')
        cy.get('.m-btn--checkPrice')
        cy.get('.m-inputText--right').type('3')
        cy.get('.m-btn--checkPrice').click()
        cy.contains('49.08')
    })


});