import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('45 Suggestion - Part Number', () => {

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click the displayed [SFJ3-10 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-10 is selected.
  */
  it('Suggestion - Part Number UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click the displayed [SFJ3-10 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-10 is selected.
  */
  it('Suggestion - Part Number DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click the displayed [SFJ3-10 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-10 is selected.
  */
  it('Suggestion - Part Number IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })
  
  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click the displayed [SFJ3-10 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-10 is selected.
  */
  it('Suggestion - Part Number FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })

});