import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('64 Cart Recalculation ', () => {

  /*
     Description: Change the Quantity value of SFJ3-10 registered in cart from 1 to 10, click the [Update] button next to the column whose the value is changed.

 
     Test Steps:
         1. Recalculation is executed, and the values of Unit price and Subtotal are updated. *Only Subtotal may be updated.
         2. Confirm the value set in Subtotal after recalculation.
         The value of Subtotal is Unit price*Quantity.
 
   */
  it('Cart Recalculation UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    
    cy.deleteAllCart()
    
    cy.addToCart('SFJ3-100', '1')
    cy.contains('SFJ3-100')
    cy.get('.lc-cart').click()

    cy.url().should('contains', 'misumi-ec.com/cart/');
    cy.get('.quantityBoxWrap').click();
    cy.get('[data-mypage-quantity="input"]').clear().type('10');
    cy.get('.button--reload').click();
    cy.contains('32.30')
  })

  it('Cart Recalculation DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    
    cy.deleteAllCart()
    
    cy.addToCart('SFJ3-100', '1')
    cy.contains('SFJ3-100')
    cy.get('.lc-cart').click()

    cy.url().should('contains', 'misumi-ec.com/cart/');
    cy.get('.quantityBoxWrap').click();
    cy.get('[data-mypage-quantity="input"]').clear().type('10');
    cy.get('.button--reload').click();
    cy.contains('32.30')
  })

  it('Cart Recalculation IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.deleteAllCart()
    
    cy.addToCart('SFJ3-100', '1')
    cy.contains('SFJ3-100')
    cy.get('.lc-cart').click()

    cy.url().should('contains', 'misumi-ec.com/cart/');
    cy.get('.quantityBoxWrap').click();
    cy.get('[data-mypage-quantity="input"]').clear().type('10');
    cy.get('.button--reload').click();
    cy.contains('32.30')
  })

  it('Cart Recalculation FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.deleteAllCart()
    
    cy.addToCart('SFJ3-100', '1')
    cy.contains('SFJ3-100')
    cy.get('.lc-cart').click()

    cy.url().should('contains', 'misumi-ec.com/cart/');
    cy.get('.quantityBoxWrap').click();
    cy.get('[data-mypage-quantity="input"]').clear().type('10');
    cy.get('.button--reload').click();
    cy.contains('32.30')
  })

});