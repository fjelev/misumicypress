import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('38 Search Window Part Number', () => {

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click [Search] button.
    Test Steps:
        1. Search result: SFJ-3 is displayed in Part Number/Type of page (MISUMI Top Page> Search results).
  */
  it('Search Window Part Number UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click [Search] button.
    Test Steps:
        1. Search result: SFJ-3 is displayed in Part Number/Type of page (MISUMI Top Page> Search results).
  */
  it('Search Window Part Number DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click [Search] button.
    Test Steps:
        1. Search result: SFJ-3 is displayed in Part Number/Type of page (MISUMI Top Page> Search results).
  */
  it('Search Window Part Number IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
  })
  
  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click [Search] button.
    Test Steps:
        1. Search result: SFJ-3 is displayed in Part Number/Type of page (MISUMI Top Page> Search results).
  */
  it('Search Window Part Number FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
  })

});