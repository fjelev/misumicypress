import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('39 Search Result Search Window', () => {

  /*
    Description: Search window - Click the [SFJ3-10] image or link of Part Number / Type displayed as the search result of [SFJ3-10].
    Test Steps:
        1. Part number of product: product details page is displayed which SFJ3-10 is selected on.
  */
  it('Search Result Search Window UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
    cy.get('[data-clickable="speclink"]').invoke('removeAttr', 'target').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: Search window - Click the [SFJ3-10] image or link of Part Number / Type displayed as the search result of [SFJ3-10].
    Test Steps:
        1. Part number of product: product details page is displayed which SFJ3-10 is selected on.
  */
  it('Search Result Search Window DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
    cy.get('[data-clickable="speclink"]').invoke('removeAttr', 'target').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: Search window - Click the [SFJ3-10] image or link of Part Number / Type displayed as the search result of [SFJ3-10].
    Test Steps:
        1. Part number of product: product details page is displayed which SFJ3-10 is selected on.
  */
  it('Search Result Search Window IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
    cy.get('[data-clickable="speclink"]').invoke('removeAttr', 'target').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: Search window - Click the [SFJ3-10] image or link of Part Number / Type displayed as the search result of [SFJ3-10].
    Test Steps:
        1. Part number of product: product details page is displayed which SFJ3-10 is selected on.
  */
  it('Search Result Search Window FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
    cy.get('[data-clickable="speclink"]').invoke('removeAttr', 'target').click()
    cy.contains('SFJ3-100')
  })

});