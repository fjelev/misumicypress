import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('63 Cart Quotation ', () => {

    /*
     Description: Click the [Request a Quote] button on the top of the page after selecting the following items on the cart page.
                <Select as>
                Shipping Preference：Specify Ship Dates
                Number of selected items  ：1 item
                Selected part number      ：SFJ3-10
                Selected quantity         ：1

 
     Test Steps:
         1. Cart: Quotation: the page is displayed, and SFJ3-10 is set to MISUMI Part No. and 1 is set to Quantity.
         2. Click the [Request a Quote] button on the bottom of the page after selecting the following items on the cart page.
                <Select as>
            Shipping Preference：Specify Ship Dates
            Number of selected items  ：1 item
            Selected part number      ：SFJ3-10
            Selected quantity         ：1

 
             3. Quotation: the page is displayed, and SFJ3-10 is set to MISUMI Part No. and 1 is set to Quantity.
 
 
   */
    it('Cart Quotation UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        
        cy.deleteAllCart()
        
        cy.addToCart('SFJ3-100', '1')
        cy.contains('SFJ3-100')
        cy.get('.lc-cart').click()
        
        cy.get('.productCounter__count').invoke('text').then(x => {
            cy.log(parseFloat(x))
            const items = parseFloat(x)
            cy.log(items)
            if (items === 1) {
                cy.log('1 item')
                cy.contains('SFJ3-100')
                cy.get('select').select('STANDARD SHIPMENT')
                cy.get('table > tbody > tr:nth-child(3) > td > div > label > input').click()
            }
        })
        
        cy.intercept('POST', '**mypage/post_cart**').as('orderPage')
        cy.get('.button--estimate').click()
        cy.wait("@orderPage")

        cy.contains('SFJ3-100')
    })

    it('Cart Quotation DE', () => {
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        
        cy.deleteAllCart()
        
        cy.addToCart('SFJ3-100', '1')
        cy.contains('SFJ3-100')
        cy.get('.lc-cart').click()
        
        cy.get('.productCounter__count').invoke('text').then(x => {
            cy.log(parseFloat(x))
            const items = parseFloat(x)
            cy.log(items)
            if (items === 1) {
                cy.log('1 item')
                cy.contains('SFJ3-100')
                cy.get('select').select('STANDARD SHIPMENT')
                cy.get('table > tbody > tr:nth-child(3) > td > div > label > input').click()
            }
        })
        
        cy.intercept('POST', '**mypage/post_cart**').as('orderPage')
        cy.get('.button--estimate').click()
        cy.wait("@orderPage")

        cy.contains('SFJ3-100')
    })

    it('Cart Quotation IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        
        cy.deleteAllCart()
        
        cy.addToCart('SFJ3-100', '1')
        cy.contains('SFJ3-100')
        cy.get('.lc-cart').click()
        
        cy.get('.productCounter__count').invoke('text').then(x => {
            cy.log(parseFloat(x))
            const items = parseFloat(x)
            cy.log(items)
            if (items === 1) {
                cy.log('1 item')
                cy.contains('SFJ3-100')
                cy.get('select').select('STANDARD SHIPMENT')
                cy.get('table > tbody > tr:nth-child(3) > td > div > label > input').click()
            }
        })
        
        cy.intercept('POST', '**mypage/post_cart**').as('orderPage')
        cy.get('.button--estimate').click()
        cy.wait("@orderPage")

        cy.contains('SFJ3-100')
    })

    it('Cart Quotation FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        
        cy.deleteAllCart()
        
        cy.addToCart('SFJ3-100', '1')
        cy.contains('SFJ3-100')
        cy.get('.lc-cart').click()
        
        cy.get('.productCounter__count').invoke('text').then(x => {
            cy.log(parseFloat(x))
            const items = parseFloat(x)
            cy.log(items)
            if (items === 1) {
                cy.log('1 item')
                cy.contains('SFJ3-100')
                cy.get('select').select('STANDARD SHIPMENT')
                cy.get('table > tbody > tr:nth-child(3) > td > div > label > input').click()
            }
        })
        
        cy.intercept('POST', '**mypage/post_cart**').as('orderPage')
        cy.get('.button--estimate').click()
        cy.wait("@orderPage")

        cy.contains('SFJ3-100')
    })

});