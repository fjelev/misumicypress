import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('29 Product Details', () => {
/*
  Description: Click [Straight] on the specification search page [Linear Shafts].
  Test Steps:
      1. Product details page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight)
*/ 
  it('Product Details UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Automation Components').click({force: true})
    cy.contains('Search by category of Automation Components')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linear Shafts').click()
    cy.contains('Straight')
    cy.contains('Straight').invoke('removeAttr', 'target').click()
  })

/*
  Description: Click [Straight] on the specification search page [Linear Shafts].
  Test Steps:
      1. Product details page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight)
*/ 
  it('Product Details DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Mechanische Komponenten').click({force: true})
    cy.contains('Suche nach Kategorie der Mechanische Komponenten')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linearwellen').click()
    cy.contains('Gerade')
    cy.contains('Linearwellen / gerade / Bearbeitung wählbar').invoke('removeAttr', 'target').click()
    cy.contains('Linearwellen / gerade / Bearbeitung wählbar')
  })

/*
  Description: Click [Straight] on the specification search page [Linear Shafts].
  Test Steps:
      1. Product details page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight)
*/ 
  it('Product Details IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('[href="/vona2/mech/"]').click({force: true})
    cy.contains('Eseguire la ricerca specificando la categoria di Componenti meccanici')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Alberi lineari').click()
    cy.contains('Dritto')
    cy.contains('Dritti').invoke('removeAttr', 'target').click()
    cy.contains('Dritti')
  })

/*
  Description: Click [Straight] on the specification search page [Linear Shafts].
  Test Steps:
      1. Product details page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight)
*/ 
  it('Product Details FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Composants mécaniques').click({force: true})
    cy.contains('Rechercher en précisant la catégorie de Composants mécaniques')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Arbres linéaires').click()
    cy.contains('Droit')
    cy.contains('Droit').invoke('removeAttr', 'target').click()
    cy.contains('Droit')
  })

});