import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('28 Specification Search', () => {
/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Specification Search UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Automation Components').click({force: true})
    cy.contains('Search by category of Automation Components')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linear Shafts').click()
    cy.contains('Straight')
  })

/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Specification Search DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Mechanische Komponenten').click({force: true})
    cy.contains('Suche nach Kategorie der Mechanische Komponenten')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linearwellen').click()
    cy.contains('Gerade')
  })

/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Specification Search IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('[href="/vona2/mech/"]').click({force: true})
    cy.contains('Eseguire la ricerca specificando la categoria di Componenti meccanici')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Alberi lineari').click()
    cy.contains('Dritto')
  })

/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Specification Search FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Composants mécaniques').click({force: true})
    cy.contains('Rechercher en précisant la catégorie de Composants mécaniques')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Arbres linéaires').click()
    cy.contains('Droit')
  })
});