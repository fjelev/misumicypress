import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('46 PDF', () => {
  const pdfUK = 'https://uk.misumi-ec.com/pdf/fa/2014/P1_0117-0118_F01_UK_001.pdf'
  const pdfDE = 'https://de.misumi-ec.com/pdf/fa/2014/P1_0117-0118_F01_DE_001.pdf'
  const pdfIT = 'https://it.misumi-ec.com/pdf/fa/2014/P1_0117-0118_F01_IT_001.pdf'
  const pdfFR = 'https://fr.misumi-ec.com/pdf/fa/2014/P1_0117-0118_F01_FR_001.pdf'
  /*
    Description: Part number of product SFJ3-10: product details page - Click the [PDF] button at the top right of the page.
    Test Steps:
        1. PDF is displayed.
  */
  it('PDF UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

    cy.get('.m-btn--pdfPage').invoke('attr', 'href').then(($pdfUrl1) => {
      const pdfUrl1 = $pdfUrl1
      cy.log(url_uk + pdfUrl1.substring(18))
      cy.request({
        url: url_uk + pdfUrl1.substring(18),
        encoding: 'binary',
      }).then((response) => {
        cy.log(response.headers['content-type'])
        cy.expect(response.headers['content-type']).eq('application/pdf')
      })
    })
  })

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [PDF] button at the top right of the page.
    Test Steps:
        1. PDF is displayed.
  */
  it('PDF DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    // cy.searchPart('SFJ3-100')
    // cy.contains('SFJ3-100')

    cy.request({
      url: pdfDE,
      encoding: 'binary',
    }).then((response) => {
      cy.log(response.headers['content-type'])
      cy.expect(response.headers['content-type']).eq('application/pdf')
    })
  })

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [PDF] button at the top right of the page.
    Test Steps:
        1. PDF is displayed.
  */
  it('PDF IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    // cy.searchPart('SFJ3-100')
    // cy.contains('SFJ3-100')

    cy.request({
      url: pdfIT,
      encoding: 'binary',
    }).then((response) => {
      cy.log(response.headers['content-type'])
      cy.expect(response.headers['content-type']).eq('application/pdf')
    })
  })

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [PDF] button at the top right of the page.
    Test Steps:
        1. PDF is displayed.
  */
  it('PDF FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    // cy.searchPart('SFJ3-100')
    // cy.contains('SFJ3-100')
   
    cy.request({
      url: pdfFR,
      encoding: 'binary',
    }).then((response) => {
      cy.log(response.headers['content-type'])
      cy.expect(response.headers['content-type']).eq('application/pdf')
    })
  })
});