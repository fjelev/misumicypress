import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('31 Specification Item', () => {
    const confItem = () => {
        cy.intercept('GET', '**typeCode=SFJ**').as('configurePart')
        cy.intercept('POST', '**/api/v1/log/add**').as('lengthItem')
        cy.get('[data-unit="mm"]').type('10').type('{enter}')
        cy.wait(3000)
        cy.wait("@lengthItem")
        cy.intercept('POST', '**/api/v1/log/add**').as('postSFJ')
        cy.get('[data-sitelog-value="SFJ"').click({
            force: true
        })
        cy.wait(1000)
        cy.wait("@postSFJ")
        cy.intercept('POST', '**/api/v1/log/add**').as('shaftDIA')
        cy.get('[data-sitelog-value="3"]').click({
            force: true
        })
        cy.wait(1000)
        cy.wait("@shaftDIA")
        cy.get('[data-sitelog-value="LKC"]').click({
            force: true
        })
        cy.wait(3000)
        cy.wait("@configurePart")
        cy.contains('SFJ3-10-LKC').should('exist');
    }


    /*
        Description: Select the following specification items on product details page.

        [Regular item]
        Shaft Dia. D(φ)：3
        Length L(mm)    ：10
        Type            ：SFJ

        [Alteration]
        Changes L Dimension Tolerance [LKC](mm)：LKC

        Test Steps:
            1. The part number of product is fixed as SFJ3-10-LKC.
    */
    it('Specification Item UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.contains('Automation Components').click({
            force: true
        })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Linear Shafts').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")
        cy.contains('MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight')

        if (!url_uk.includes("stg0")) {
            confItem()
        }
    })

    /*
        Description: Select the following specification items on product details page.

        [Regular item]
        Shaft Dia. D(φ)：3
        Length L(mm)    ：10
        Type            ：SFJ

        [Alteration]
        Changes L Dimension Tolerance [LKC](mm)：LKC

        Test Steps:
            1. The part number of product is fixed as SFJ3-10-LKC.
    */
    it('Specification Item DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.contains('Mechanische Komponenten').click({
            force: true
        })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Linearwellen').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('MISUMI Startseite> Mechanische Komponenten> Linearbewegung> Linearwellen> Linearwellen / gerade / Bearbeitung wählbar')

        if (!url_de.includes("stg0")) {
            confItem()
        }
    })

    /*
        Description: Select the following specification items on product details page.

        [Regular item]
        Shaft Dia. D(φ)：3
        Length L(mm)    ：10
        Type            ：SFJ

        [Alteration]
        Changes L Dimension Tolerance [LKC](mm)：LKC

        Test Steps:
            1. The part number of product is fixed as SFJ3-10-LKC.
    */
    it('Specification Item IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('[href="/vona2/mech/"]').click({
            force: true
        })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Alberi lineari').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('Pagina principale MISUMI> Componenti meccanici> Movimentazione lineare> Alberi lineari> Dritti')

        if (!url_it.includes("stg0")) {
            confItem()
        }
    })

    /*
        Description: Select the following specification items on product details page.

        [Regular item]
        Shaft Dia. D(φ)：3
        Length L(mm)    ：10
        Type            ：SFJ

        [Alteration]
        Changes L Dimension Tolerance [LKC](mm)：LKC

        Test Steps:
            1. The part number of product is fixed as SFJ3-10-LKC.
    */
    it('Specification Item FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('[href="/vona2/mech/"]').click({
            force: true
        })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Arbres linéaires').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('Haut de la page MISUMI> Composants mécaniques> Mouvement linéaire> Arbres linéaires> Droit')

        if (!url_fr.includes("stg0")) {
            confItem()
        }
    })

    /*
      Description: Click the [Clear all] button at the top of the specification item with the part number of product fixed as SFJ3-10-LKC.

      Test Steps:
          1. Fixed part number of product is canceled.
    */
    it('Specification Item UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.contains('Automation Components').click({
            force: true
        })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Linear Shafts').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")
        cy.contains('MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight')

        if (!url_uk.includes("stg0")) {
            confItem()
        }
    })

    /*
      Description: Click the [Clear all] button at the top of the specification item with the part number of product fixed as SFJ3-10-LKC.

      Test Steps:
          1. Fixed part number of product is canceled.
    */
    it('Specification Item DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.contains('Mechanische Komponenten').click({
            force: true
        })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Linearwellen').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('MISUMI Startseite> Mechanische Komponenten> Linearbewegung> Linearwellen> Linearwellen / gerade / Bearbeitung wählbar')

        if (!url_de.includes("stg0")) {
            confItem()
        }
    })

    /*
      Description: Click the [Clear all] button at the top of the specification item with the part number of product fixed as SFJ3-10-LKC.

      Test Steps:
          1. Fixed part number of product is canceled.
    */
    it('Specification Item IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('[href="/vona2/mech/"]').click({
            force: true
        })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Alberi lineari').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('Pagina principale MISUMI> Componenti meccanici> Movimentazione lineare> Alberi lineari> Dritti')

        if (!url_it.includes("stg0")) {
            confItem()
        }
    })

    /*
      Description: Click the [Clear all] button at the top of the specification item with the part number of product fixed as SFJ3-10-LKC.

      Test Steps:
          1. Fixed part number of product is canceled.
    */
    it('Specification Item FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('[href="/vona2/mech/"]').click({
            force: true
        })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Arbres linéaires').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('Haut de la page MISUMI> Composants mécaniques> Mouvement linéaire> Arbres linéaires> Droit')

        if (!url_fr.includes("stg0")) {
            confItem()
        }
    })

});