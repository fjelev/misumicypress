import {
    beforeEach
} from 'mocha';
import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;
describe('36 Add To Cart', () => {
    const partName = 'BH6'
    /*
        Description: Enter 1 in the quantity of modal for quotation  and order of suggested product without information posted. Click [Add to Cart] button.
        Test Steps:
            1. Modal representing that the product is added to the cart is displayed. (The item has been added to the shopping cart.)
    */
    it('Add To Cart UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
    })

    /*
        Description: Enter 1 in the quantity of modal for quotation  and order of suggested product without information posted. Click [Add to Cart] button.
        Test Steps:
            1. Modal representing that the product is added to the cart is displayed. (The item has been added to the shopping cart.)
    */
    it('Add To Cart DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
    })

    /*
        Description: Enter 1 in the quantity of modal for quotation  and order of suggested product without information posted. Click [Add to Cart] button.
        Test Steps:
            1. Modal representing that the product is added to the cart is displayed. (The item has been added to the shopping cart.)
    */
    it('Add To Cart IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.addToCart(partName, 1)
    })

    /*
        Description: Enter 1 in the quantity of modal for quotation  and order of suggested product without information posted. Click [Add to Cart] button.
        Test Steps:
            1. Modal representing that the product is added to the cart is displayed. (The item has been added to the shopping cart.)
    */
    it('Add To Cart FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
    })

    /*
        Description: Click the [View Cart] button which shows that a product is added to cart.
        Test Steps:
            1. The cart page is displayed and BH6 is added to the Product specification with Quantity 1 (Cart)
    */
    it('Add To Cart UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
        cy.get('.lc-cart').click()
        cy.contains('BH6')
    })

    /*
        Description: Click the [View Cart] button which shows that a product is added to cart.
        Test Steps:
            1. The cart page is displayed and BH6 is added to the Product specification with Quantity 1 (Cart)
    */
    it('Add To Cart DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
        cy.get('.lc-cart').click()
        cy.contains('BH6')
    })

    /*
        Description: Click the [View Cart] button which shows that a product is added to cart.
        Test Steps:
            1. The cart page is displayed and BH6 is added to the Product specification with Quantity 1 (Cart)
    */
    it('Add To Cart IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
        cy.get('.lc-cart').click()
        cy.contains('BH6')
    })

    /*
        Description: Click the [View Cart] button which shows that a product is added to cart.
        Test Steps:
            1. The cart page is displayed and BH6 is added to the Product specification with Quantity 1 (Cart)
    */
    it('Add To Cart FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
        cy.get('.lc-cart').click()
        cy.contains('BH6')
    })
})