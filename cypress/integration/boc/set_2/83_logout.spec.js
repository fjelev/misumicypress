import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('83 Logout ', () => {

  /*
    Description: eCatalog top page - expand the menu of [login user name] on the top right of the page and click [Logout] link.
    Test Steps:
        1. Logout process is executed, and the notation of [login user name] button changes to Login.

  */
  it('Logout UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('.lc-user').trigger('mouseover')
    cy.get('[data-login="logoutLink"]').click({force: true})
    cy.contains('› Login/Register')
  })

  it('Logout DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('.lc-user').trigger('mouseover')
    cy.get('[data-login="logoutLink"]').click({force: true})
    cy.contains('› Login/Registrierung')
  })

  it('Logout IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('.lc-user').trigger('mouseover')
    cy.get('[data-login="logoutLink"]').click({force: true})
    cy.contains('› Login/Registrati')
  })

  it('Logout FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('.lc-user').trigger('mouseover')
    cy.get('[data-login="logoutLink"]').click({force: true})
    cy.contains('› Se connecter/Crèer un compte')
  })

});