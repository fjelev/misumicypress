 import Login from '../pom/login';
 import MyPage from '../pom/myPage';
 import SideMenu from '../pom/sideMenu';
 import UserMenu from '../pom/userMenu';
 
 const authUser = require('../../../fixtures/auth-user.json');
 const login = new Login();
 const userMenu = new UserMenu();
 const sideMenu = new SideMenu();
 const myPage = new MyPage();
 const {
     username,
     password,
     url_uk,
     url_de,
     url_fr,
     url_it
 } = authUser;
 
 describe('68 My Page Order History ', () => {
     
   /*
     Description: My page top page - Click the [Order History] link on the left side of the page.

     Test Steps:
         1. Order history: the page is displayed. (MISUMI Home>My MISUMI>Order History)
   */
  it('My Page Order History UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page Order History DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page Order History IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page Order History FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })
 
 });