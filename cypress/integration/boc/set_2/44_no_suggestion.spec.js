import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('44 Suggestion No Hits', () => {

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

});