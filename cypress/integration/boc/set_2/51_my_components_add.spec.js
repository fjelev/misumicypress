import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('51 Add to My Components', () => {

  /*
    Description: part number of product SFJ3-10: click the [Save] button on the product details page.
    Test Steps:
        1. Modal representing that the product is added to My components is displayed. (Added to My Components)
        Click the [My Components] button of the modal which shows adding a product to My components.
        Main folder page of My components is displayed, and SFJ3-10 is added. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)

  */
  it('Add to My Components UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#add_parts').click()
    cy.contains('Added to My Components')
    cy.wait(3000)
    cy.get('[href="/mypage/parts.html"]').click()
    cy.contains('SFJ3-100')
  })


  it('Add to My Components DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#add_parts').click()
    cy.wait(3000)
    cy.get('[href="/mypage/parts.html"]').click()
    cy.contains('SFJ3-100')
  })

  it('Add to My Components IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#add_parts').click()
    cy.wait(3000)
    cy.get('[href="/mypage/parts.html"]').click()
    cy.contains('SFJ3-100')
  })

  it('Add to My Components FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#add_parts').click()
    cy.wait(3000)
    cy.get('[href="/mypage/parts.html"]').click()
    cy.contains('SFJ3-100')
  })
});