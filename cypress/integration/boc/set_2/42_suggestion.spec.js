import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('42 Search Window Keyword', () => {

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the displayed [linear shaft] link which is suggested.
    Test Steps:
        1. Search result: search results of Linear Shaft are displayed in Category of the page (MISUMI Startseite> Suchergebnisse).
  */
  it('Search Window Keyword UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('Linear')
    cy.contains('linear shaft').invoke('attr', 'href').then(($linearUrl1) => {
      const linearUrl1 = $linearUrl1
      cy.visit(url_uk + linearUrl1)
    })
    cy.contains('Linear Shafts')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the displayed [linear shaft] link which is suggested.
    Test Steps:
        1. Search result: search results of Linear Shaft are displayed in Category of the page (MISUMI Startseite> Suchergebnisse).
  */
  it('Search Window Keyword DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('linearb')
    cy.contains('linearbewegung').invoke('attr', 'href').then(($linearUrl1) => {
      const linearUrl1 = $linearUrl1
      cy.visit(url_de + linearUrl1)
    })
    cy.contains('Linearbewegung')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the displayed [linear shaft] link which is suggested.
    Test Steps:
        1. Search result: search results of Linear Shaft are displayed in Category of the page (MISUMI Startseite> Suchergebnisse).
  */
  it('Search Window Keyword IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('Movimentazione')
    cy.contains('movimentazione lineare').invoke('attr', 'href').then(($linearUrl1) => {
      const linearUrl1 = $linearUrl1
      cy.visit(url_it + linearUrl1)
    })
    cy.contains('Movimentazione lineare')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the displayed [linear shaft] link which is suggested.
    Test Steps:
        1. Search result: search results of Linear Shaft are displayed in Category of the page (MISUMI Startseite> Suchergebnisse).
  */
  it('Search Window Keyword FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('#keyword_input').type('linéaire')
    cy.contains('arbre linéaire').invoke('attr', 'href').then(($linearUrl1) => {
      const linearUrl1 = $linearUrl1
      cy.visit(url_fr + linearUrl1)
    })
    cy.contains('Arbre linéaire')
  })

});