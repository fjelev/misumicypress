import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('43 Search Result Suggestion', () => {

  /*
    Description: Suggestion - Click the [Linear Shafts] image of Category as the search result of [linear shafts].
    Test Steps:
        1. Specification search page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts)
  */
  it('Search Result Suggestion UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.wait(3000)
    cy.get('#keyword_input').type('Linear')
    cy.contains('Linear Shafts').invoke('removeAttr', 'target').click()
    cy.wait(1000)
    cy.contains('Linear Shafts')
  })

  /*
    Description: Suggestion - Click the [Linear Shafts] image of Category as the search result of [linear shafts].
    Test Steps:
        1. Specification search page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts)
  */
  it('Search Result Suggestion DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.wait(3000)
    cy.get('#keyword_input').type('linearwellen').type('{enter}')
    cy.contains('Linearwellen').invoke('removeAttr', 'target').click()
    cy.wait(1000)
    cy.contains('Linearwellen / gerade / Bearbeitung wählbar')
  })

  /*
    Description: Suggestion - Click the [Linear Shafts] image of Category as the search result of [linear shafts].
    Test Steps:
        1. Specification search page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts)
  */
  it('Search Result Suggestion IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.wait(3000)
    cy.get('#keyword_input').type('Alberi lineari').type('{enter}')
    cy.contains('Alberi lineari').invoke('removeAttr', 'target').click()
    cy.wait(1000)
    cy.contains('Alberi lineari')
  })

  /*
    Description: Suggestion - Click the [Linear Shafts] image of Category as the search result of [linear shafts].
    Test Steps:
        1. Specification search page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts)
  */
  it('Search Result Suggestion FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.wait(3000)
    cy.get('#keyword_input').type('Arbres linéaires').type('{enter}')
    cy.contains('Arbres linéaires').invoke('removeAttr', 'target').click()
    cy.wait(1000)
    cy.contains('Arbres linéaires')
  })

});