import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('78 My Page Change Password ', () => {

  /*
    Description: My page top page - Click the [Change the current login/password.] link on the left side of the page.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)

  */
 it('My Page Change Password UK', () => {
  cy.visit(url_uk)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.searchPart('SFJ3-100')
  cy.contains('SFJ3-100')

})

it('My Page Change Password DE', () => {
  cy.visit(url_de)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.searchPart('SFJ3-100')
  cy.contains('SFJ3-100')

})

it('My Page Change Password IT', () => {
  cy.visit(url_it)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.searchPart('SFJ3-100')
  cy.contains('SFJ3-100')

})

it('My Page Change Password FR', () => {
  cy.visit(url_fr)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.searchPart('SFJ3-100')
  cy.contains('SFJ3-100')

})

});