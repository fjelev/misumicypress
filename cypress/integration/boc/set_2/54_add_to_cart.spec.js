import Login from '../pom/login';
import { getFirstAvailableUser, releaseUser } from '../../../support/commands'

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const {
    url_uk,
    url_de,
    url_fr,
    url_it,
    userpassSTG0,
    userpass
} = authUser;

describe('54 Add To Cart', () => {
    let currUser;
    let userPass;
    beforeEach(() => {
        cy.visit(url_uk)
        const getURLTxt = url_uk;
        if (getURLTxt.includes("stg0")) {
            userPass = userpassSTG0
        } else {
            userPass = userpass
        }
        currUser = getFirstAvailableUser()
        cy.log(currUser, 'Available user')
    })
    afterEach(() => {
        releaseUser(currUser);
        cy.log(currUser, 'released User')
    });
    /*
      Description: Part number of product SFJ3-10: click the [Add to Cart] button with Order qty. set to 1 on the right side of product details page.

      Test Steps:
          1. Modal representing that the product is added to cart is displayed.
              (The item has been added to the shopping cart.)
          2. Click [View Cart] button which shows a product is added to cart.
              Cart: the page is displayed, and SFJ3-10 is added to Product specification with Quantity 1.
              (Cart)


    */
    it('Add To Cart UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.cartLogin(currUser, userPass)
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = parseFloat(x)
            cy.log(items)
            cy.expect(items).eq(0)
        })
        cy.wait(3000)
        cy.addToCart('SFJ3-100', '1')
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = Number(x)
            cy.log(items)
            cy.expect(items).eq(1)
        })
    })

    it('Add To Cart DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.cartLogin(currUser, userPass)
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = parseFloat(x)
            cy.log(items)
            cy.expect(items).eq(0)
        })
        cy.wait(3000)
        cy.addToCart('SFJ3-100', '1')
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = Number(x)
            cy.log(items)
            cy.expect(items).eq(1)
        })
    })

    it('Add To Cart IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.cartLogin(currUser, userPass)
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = parseFloat(x)
            cy.log(items)
            cy.expect(items).eq(0)
        })
        cy.wait(3000)
        cy.addToCart('SFJ3-100', '1')
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = Number(x)
            cy.log(items)
            cy.expect(items).eq(1)
        })
    })

    it('Add To Cart FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.cartLogin(currUser, userPass)
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = parseFloat(x)
            cy.log(items)
            cy.expect(items).eq(0)
        })
        cy.wait(3000)
        cy.addToCart('SFJ3-100', '1')
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = Number(x)
            cy.log(items)
            cy.expect(items).eq(1)
        })
    })
});