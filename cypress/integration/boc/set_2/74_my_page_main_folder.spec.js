import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('74 My Page CAD Download ', () => {

  /*
    Description: My page top page - Click the [Main folder] link on the left side of the page.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)

  */
  it('My Page CAD Download UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page CAD Download DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page CAD Download IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page CAD Download FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })
});