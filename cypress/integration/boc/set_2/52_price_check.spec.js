import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('52 Price Check', () => {

  /*
    Description: Part number of product SFJ3-10: click the [Check Price / discount rate] button with Order Qty. 
                                        set to 1 and Unit price to --- on the right side of product details page.
    Test Steps:
        1. Values must be set for three items: Unit price, Total, Shipping Days

  */
  it('Price Check UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('.m-btn--checkPrice').click()
    cy.get('.mc-num').contains('3.40')
  })

  it('Price Check DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('.m-btn--checkPrice').click()
    cy.get('.mc-num').contains('3.40')
  })

  it('Price Check IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('.m-btn--checkPrice').click()
    cy.get('.mc-num').contains('3.40')
  })


  it('Price Check FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('.m-btn--checkPrice').click()
    cy.get('.mc-num').contains('3.40')
  })


});