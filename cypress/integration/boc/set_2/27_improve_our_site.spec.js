import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('27 Improve Our Site', () => {
/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Improve Our Site UK', () => {
    cy.visit(url_uk)
            cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

    cy.contains('Brand Overview').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.get('#commentLink').click()
    cy.contains('Please give us a feedback on the MISUMI e-Catalog.')
  })

/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Improve Our Site DE', () => {
    cy.visit(url_de)
            cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

    cy.contains('Alle Marken').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.get('#commentLink').click()
    cy.contains('Bitte geben Sie uns Ihr Feedback zu unserem MISUMI e-Katalog.')
  })

/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Improve Our Site IT', () => {
    cy.visit(url_it)
            cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

    cy.contains('Tutti i marchi').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.get('#commentLink').click()
    cy.contains('É gradito un commento sull\'e-Catalog MISUMI')
  })

/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Improve Our Site FR', () => {
    cy.visit(url_fr)
            cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

    cy.contains('Toutes les marques').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.get('#commentLink').click()
    cy.contains('Donnez-nous votre avis sur le catalogue en ligne MISUMI.')
  })

});