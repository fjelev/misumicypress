import { beforeEach } from 'mocha';
import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('32 Search parts', () => {
    beforeEach(() => {
        cy.clearCookies()
        cy.clearLocalStorage()
    })
/*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
*/ 
    it('Search Parts UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type('LHQRW10')
        cy.contains('LHQRW10 [MISUMI Discontinued product, limited availability]');
    })

/*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
*/ 
    it('Search parts DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type('LHQRW10')
        cy.wait(1000)
        cy.contains('LHQRW10 [MISUMI Abgekündigtes Produkt, begrenzte Verfügbarkeit]');
    })

/*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
*/ 
    it('Search parts IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type('LHQRW10')
        cy.contains('LHQRW10 [MISUMI Prodotto fuori produzione, disponibilità limitata]');
    })

/*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
*/ 
    it('Search parts FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.get('#keyword_input').type('LHQRW10')
        cy.contains('LHQRW10 [MISUMI Produit suspendu, disponibilité limitée]');
    })

/*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
*/ 
    it('Search Parts Open Part Number UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('LHQRW10')
        cy.url().should('include', 'LHQRW10')
        cy.contains('Linear Bushings with Grease Nipples')
    })

/*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
*/ 
    it('Search Parts Open Part Number DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('LHQRW10')
        cy.url().should('include', 'LHQRW10')
        cy.changeLanguage('de')
        cy.contains('Linearkugellager mit Schmiernippel')
    })

/*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
*/ 
    it('Search Parts Open Part Number IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('LHQRW10')
        cy.url().should('include', 'LHQRW10')
        cy.changeLanguage('it')
        cy.contains('Boccole lineari con ingrassatore')
    })

/*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
*/ 
    it('Search Parts Open Part Number FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        cy.searchPart('LHQRW10')
        cy.url().should('include', 'LHQRW10')
        cy.changeLanguage('fr')
        cy.contains('Manchons linéaires à graisseur')
    })
}); 