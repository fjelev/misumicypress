import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('48 3D Preview', () => {

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [3D Preview] button at the top right of the page.
    Test Steps:
        1. 3D preview is displayed.
  */
  it('3D Preview UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#Tab_preview').click()
    cy.wait(5000)
    cy.get('#Tab_preview_contents')
  })

  it('3D Preview DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#Tab_preview').click()
    cy.wait(5000)
    cy.get('#Tab_preview_contents')
  })

  it('3D Preview IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#Tab_preview').click()
    cy.wait(5000)
    cy.get('#Tab_preview_contents')
  })

  it('3D Preview FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#Tab_preview').click()
    cy.wait(5000)
    cy.get('#Tab_preview_contents')
  })

});