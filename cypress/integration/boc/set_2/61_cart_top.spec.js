import Login from '../pom/login';
import { getFirstAvailableUser, releaseUser } from '../../../support/commands'

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const {
    url_uk,
    url_de,
    url_fr,
    url_it,
    userpassSTG0,
    userpass
} = authUser;

describe('61 Cart TOP', () => {
    let currUser;
    let userPass;
    beforeEach(() => {
        cy.visit(url_uk)
        const getURLTxt = url_uk;
        if (getURLTxt.includes("stg0")) {
            userPass = userpassSTG0
        } else {
            userPass = userpass
        }
        currUser = getFirstAvailableUser()
        cy.log(currUser, 'Available user')
    })
    afterEach(() => {
        releaseUser(currUser);
        cy.log(currUser, 'released User')
    });

  /*
    Description: Confirm the cart page with 11 or more items registered in the cart.
                *The display method of the cart page is optional.
                Also, when the number of products registered in cart is less than 11 items, add any part numbers and make it not less than 11 items.

    Test Steps:
        1. Cart: check the entire page to confirm there are no garbled characters and display collapse. (Cart)
  */
    it('Cart TOP UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.cartLogin(currUser, userPass)
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = parseFloat(x)
            cy.log(items)
            cy.expect(items).eq(0)
        })
        cy.wait(3000)
        for(let i=0; i < 11; i++) cy.addToCart('SFJ3-100', '1')
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = Number(x)
            cy.log(items)
            cy.expect(items).eq(11)
        })
    })

    it('Cart TOP DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.cartLogin(currUser, userPass)
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = parseFloat(x)
            cy.log(items)
            cy.expect(items).eq(0)
        })
        cy.wait(3000)
        for(let i=0; i < 11; i++) cy.addToCart('SFJ3-100', '1')
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = Number(x)
            cy.log(items)
            cy.expect(items).eq(11)
        })
    })

    it('Cart TOP IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.cartLogin(currUser, userPass)
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = parseFloat(x)
            cy.log(items)
            cy.expect(items).eq(0)
        })
        cy.wait(3000)
        for(let i=0; i < 11; i++) cy.addToCart('SFJ3-100', '1')
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = Number(x)
            cy.log(items)
            cy.expect(items).eq(11)
        })
    })

    it('Cart TOP FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.cartLogin(currUser, userPass)
        cy.wait("@initialLoad")
        cy.deleteAllCart()
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = parseFloat(x)
            cy.log(items)
            cy.expect(items).eq(0)
        })
        cy.wait(3000)
        for(let i=0; i < 11; i++) cy.addToCart('SFJ3-100', '1')
        cy.get('[data-common="cartCount"]').invoke('text').then(x => {
            const items = Number(x)
            cy.log(items)
            cy.expect(items).eq(11)
        })
    })
});