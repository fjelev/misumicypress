const authUser = require('../../../fixtures/auth-user.json');

class Login {
    signIn() {
        const { username, password } = authUser;
        this.acceptCookies().click()
        const loginMenuButton = cy.get('[data-login="loginButton"]');
        loginMenuButton.click()
        this.username().type(username)
        this.password().type(password)
        this.signInButton().click()
        return this
    }

    acceptCookies() {
        return cy.get('.l-cookieNotification__accept');
    }

    essentialCookies() {
        return cy.get('.l-cookieNotification__reject');
    }

    username() {
        return cy.get('[data-login="userid"]')
    }

    password() {
        return cy.get('[data-login="password"]')
    }

    signInButton(){
        return cy.get('[data-login="submit"]')
    }
}

export default Login;
