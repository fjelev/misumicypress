const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;
describe('23 Brand Category', () => {
  
  /*
    Description: Click [Self-Drill Screws ] link in the detail page of AX BRAIN.
    Test Steps:
        1. Category page of AX BRAIN Self-Drill Screws is displayed. (uk.misumi-ec.com/vona2/maker/axbrain/mech_screw/M3301000000/M3301180000/)
  */ 
  it('Brand Category UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Brand Overview').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('AX BRAIN').click()
    cy.url().should('include', 'uk.misumi-ec.com/vona2/maker/axbrain')
    cy.get('[src*=".misumi-ec.com/linked/material/mech/category/M3301180000.jpg?$category_first$"]').click()
    cy.url().should('include', 'uk.misumi-ec.com/vona2/maker/axbrain/mech_screw/M3301000000/M3301180000')
  })

  
  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (de.misumi-ec.com/vona2/maker/axbrain/)
  */ 
 it('Brand Category DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Alle Marken').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('AX BRAIN').click()
    cy.url().should('include', 'de.misumi-ec.com/vona2/maker/axbrain')
    cy.get('[src*=".misumi-ec.com/linked/material/mech/category/M3301180000.jpg?$category_first$"]').click()
    cy.url().should('include', 'de.misumi-ec.com/vona2/maker/axbrain/mech_screw/M3301000000/M3301180000')
  })

  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (it.misumi-ec.com/vona2/maker/axbrain/)
  */ 
 it('Brand Category IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Tutti i marchi').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('AX BRAIN').click()
    cy.url().should('include', 'it.misumi-ec.com/vona2/maker/axbrain')
    cy.get('[src*=".misumi-ec.com/linked/material/mech/category/M3301180000.jpg?$category_first$"]').click()
    cy.url().should('include', 'it.misumi-ec.com/vona2/maker/axbrain/mech_screw/M3301000000/M3301180000')
  })


  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (fr.misumi-ec.com/vona2/maker/axbrain/)
  */ 
 it('Brand Category FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Toutes les marques').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('AX BRAIN').click()
    cy.url().should('include', 'fr.misumi-ec.com/vona2/maker/axbrain')
    cy.get('[src*=".misumi-ec.com/linked/material/mech/category/M3301180000.jpg?$category_first$"]').click()
    cy.url().should('include', 'fr.misumi-ec.com/vona2/maker/axbrain/mech_screw/M3301000000/M3301180000')
  })
});