import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('16 Change user data', () => {
/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/ 
  it('Change user data UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    userMenu.userPanel().click()
    userMenu.changeUserData().click()
    cy.contains('Change User Information')
  })
  
/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/ 
  it('Change user data DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    userMenu.userPanel().click()
    userMenu.changeUserData().click()
    cy.contains('Benutzerdaten ändern')
  })  

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/ 
  it('Change user data IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    userMenu.userPanel().click()
    userMenu.changeUserData().click()
    cy.contains('Modifica dati utente')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/ 
  it('Change user data FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    userMenu.userPanel().click()
    userMenu.changeUserData().click()
    cy.contains('Modifier mon profil')
  })


/*
    Description: eCatalog top page - Click the [Change User Information] link on the right side of the page.
    Test Steps:
        1. User information change: the page is displayed. (Change User Information)
*/ 
it('Change user data UK', () => {
  cy.visit(url_uk)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.contains('Change user data').invoke('removeAttr', 'target').click()
  cy.contains('Change User Information')
})

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
  Test Steps:
      1. Password change: the page is displayed. (Change Password)
*/ 
it('Change user data DE', () => {
  cy.visit(url_de)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.contains('Benutzerdaten ändern').invoke('removeAttr', 'target').click()
  cy.contains('Benutzerdaten ändern')
})  

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
  Test Steps:
      1. Password change: the page is displayed. (Change Password)
*/ 
it('Change user data IT', () => {
  cy.visit(url_it)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.contains('Modifica dati utente').invoke('removeAttr', 'target').click()
  cy.contains('Modifica dati utente')
})

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
  Test Steps:
      1. Password change: the page is displayed. (Change Password)
*/ 
it('Change user data FR', () => {
  cy.visit(url_fr)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.contains('Modifier mon profil').invoke('removeAttr', 'target').click()
  cy.contains('Modifier mon profil')
})

});