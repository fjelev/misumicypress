import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const userMenu = new UserMenu();

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('17 Register user info', () => {

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Your Corporate Information] link.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    userMenu.userPanel().click()
    userMenu.corporateInfo().click()
    cy.contains('Your Corporate Information')
  })

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Your Corporate Information] link.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    userMenu.userPanel().click()
    userMenu.corporateInfo().click()
    cy.contains('Ihre registrierten Unternehmensinformationen')
  })

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Your Corporate Information] link.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    userMenu.userPanel().click()
    userMenu.corporateInfo().click()
    cy.contains('Informazioni aziendali')
  })

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Your Corporate Information] link.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    userMenu.userPanel().click()
    userMenu.corporateInfo().click()
    cy.contains('Informations société')
  })


/*
  Description: eCatalog top page - Click the [Your Corporate Information] link on the right side of the page.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Your Corporate Information').invoke('removeAttr', 'target').click()
    cy.contains('Your Corporate Information')
  })

/*
  Description: eCatalog top page - Click the [Your Corporate Information] link on the right side of the page.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Ihre registrierten Unternehmensinformationen').invoke('removeAttr', 'target').click()
    cy.contains('Ihre registrierten Unternehmensinformationen')
  })

/*
  Description: eCatalog top page - Click the [Your Corporate Information] link on the right side of the page.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Informazioni aziendali').invoke('removeAttr', 'target').click()
    cy.contains('Informazioni aziendali')
  })

/*
  Description: eCatalog top page - Click the [Your Corporate Information] link on the right side of the page.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Informations société').invoke('removeAttr', 'target').click()
    cy.contains('Informations société')
  })

});