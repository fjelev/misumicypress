const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('24 Catalogue Request', () => {
/*
  Description: catalog top page - click the [Request Catarog] link at the bottom center of the page
  Test Steps:
      1. Catalog image is displayed.(Category search)
        *This page will not be displayed in Pre environment since this does not exit in Pre environment.
*/ 
  it('Catalogue Request UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Catalogue Request').click({ force: true })
    cy.contains('Request catalogue')
  })

/*
  Description: catalog top page - click the [Request Catarog] link at the bottom center of the page
  Test Steps:
      1. Catalog image is displayed.(Category search)
        *This page will not be displayed in Pre environment since this does not exit in Pre environment.
*/ 
  it('Catalogue Request DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Katalog anfordern').click({ force: true })
    cy.contains('Kataloge anfordern')
  })


/*
  Description: catalog top page - click the [Request Catarog] link at the bottom center of the page
  Test Steps:
      1. Catalog image is displayed.(Category search)
        *This page will not be displayed in Pre environment since this does not exit in Pre environment.
*/ 
  it('Catalogue Request IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Richiedi cataloghi').click({ force: true })
    cy.contains('Richiesta di cataloghi')
  })

/*
  Description: catalog top page - click the [Request Catarog] link at the bottom center of the page
  Test Steps:
      1. Catalog image is displayed.(Category search)
        *This page will not be displayed in Pre environment since this does not exit in Pre environment.
*/ 
it('Catalogue Request FR', () => {
  cy.visit(url_fr)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")
  cy.contains('Demande de catalogues').click({ force: true })
  cy.contains('Demande de catalogues')
})

});