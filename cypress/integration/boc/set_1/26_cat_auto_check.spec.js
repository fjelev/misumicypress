const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('26 Category Check', () => {
/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Incad Idea Note UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Automation Components').click({force: true})
    cy.contains('Search by category of Automation Components')
  })

/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Incad Idea Note DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Mechanische Komponenten').click({force: true})
    cy.contains('Suche nach Kategorie der Mechanische Komponenten')
  })

/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Incad Idea Note IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('[href="/vona2/mech/"]').click({force: true})
    cy.contains('Eseguire la ricerca specificando la categoria di Componenti meccanici')
  })

/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Incad Idea Note FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Composants mécaniques').click({force: true})
    cy.contains('Rechercher en précisant la catégorie de Composants mécaniques')
  })
});