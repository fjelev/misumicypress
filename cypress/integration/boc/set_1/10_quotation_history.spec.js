const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('10 Quote History', () => {
/*
    Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
    Test Steps:
        1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
*/
  it('Order History UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Manage Quote History').click()
    cy.contains('Quotation History')
  })

/*
    Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
    Test Steps:
        1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
*/
  it('Order History DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Angebotshistorie').click()
    cy.contains('Angebotshistorie')
  })

/*
    Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
    Test Steps:
        1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
*/
  it('Order History IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Accedi a storico quotazioni').click()
    cy.contains('Storico quotazioni')
  })

/*
    Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
    Test Steps:
        1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
*/
  it('Order History FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Historique des devis').click()
    cy.contains('Historique des devis')
  })
});