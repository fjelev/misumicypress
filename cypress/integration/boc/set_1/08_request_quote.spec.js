const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('08 Request Quote', () => {

/*
    Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
    Test Steps:
        1. Quotation: the page is displayed. (New Quotation)
*/
  it('Request Quote UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Request a Quote').click()
    cy.contains('Create New Quotation')
  })

/*
    Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
    Test Steps:
        1. Quotation: the page is displayed. (New Quotation)
*/
  it('Request Quote DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Anfragen').click()
    cy.contains('Neue Anfrage')
  })

/*
    Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
    Test Steps:
        1. Quotation: the page is displayed. (New Quotation)
*/
  it('Request Quote IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Richiedi preventivo').click()
    cy.contains('Richiesta quotazione')
  })

/*
    Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
    Test Steps:
        1. Quotation: the page is displayed. (New Quotation)
*/
  it('Request Quote FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Demande de devis').click()
    cy.contains('Demande de devis')
  })

});
