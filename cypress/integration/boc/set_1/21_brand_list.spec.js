import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('21 Brand List', () => {

/*
  Description: Click [Brand overview] link at the bottom of Mega-Navi in eCatalog top page.
  Test Steps:
      1. Manufacturer List is displayed. (uk.misumi-ec.com/vona2/maker/)
*/ 
  it('Brand Details UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Brand Overview').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('rand Overview')
    cy.url().should('include', 'uk.misumi-ec.com/vona2/maker/')
  })


/*
  Description: Click [Brand overview] link at the bottom of Mega-Navi in eCatalog top page.
  Test Steps:
      1. Manufacturer List is displayed. (de.misumi-ec.com/vona2/maker/)
*/ 
  it('Brand Details DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Alle Marken').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Alle Marken')
    cy.url().should('include', 'de.misumi-ec.com/vona2/maker/')
  })


/*
  Description: Click [Brand overview] link at the bottom of Mega-Navi in eCatalog top page.
  Test Steps:
      1. Manufacturer List is displayed. (de.misumi-ec.com/vona2/maker/)
*/ 
  it('Brand Details IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Tutti i marchi').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Tutti i marchi')
    cy.url().should('include', 'it.misumi-ec.com/vona2/maker/')
  })


/*
  Description: Click [Brand overview] link at the bottom of Mega-Navi in eCatalog top page.
  Test Steps:
      1. Manufacturer List is displayed. (de.misumi-ec.com/vona2/maker/)
*/ 
  it('Brand Details FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Toutes les marques').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Toutes les marques')
    cy.url().should('include', 'fr.misumi-ec.com/vona2/maker/')
  })
});