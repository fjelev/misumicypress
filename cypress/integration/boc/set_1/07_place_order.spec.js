const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('07 Place Order', () => {

/*
    Description: eCatalog top page - Click the [Place an Order] button on the right side of the page.
    Test Steps:
        1. Order: the page is displayed.(New Order)
*/
  it('Place Order UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Place an Order').click()
    cy.contains('Create New Order')
  })

/*
    Description: eCatalog top page - Click the [Place an Order] button on the right side of the page.
    Test Steps:
        1. Order: the page is displayed.(New Order)
*/
it('Place Order DE', () => {
  cy.visit(url_de)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.contains('Bestellen').click()
  cy.contains('Neue Bestellung')
})

/*
    Description: eCatalog top page - Click the [Place an Order] button on the right side of the page.
    Test Steps:
        1. Order: the page is displayed.(New Order)
*/
it('Place Order IT', () => {
  cy.visit(url_it)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")

  cy.contains('Ordina').click()
  cy.contains('Crea richiesta d\'ordine')
})

/*
    Description: eCatalog top page - Click the [Place an Order] button on the right side of the page.
    Test Steps:
        1. Order: the page is displayed.(New Order)
*/
it('Place Order FR', () => {
  cy.visit(url_fr)
  cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  cy.login()
  cy.wait("@initialLoad")
  
  cy.contains('Commander').click()
  cy.contains('Créer une commande')
})
});