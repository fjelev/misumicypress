import CadDownload from '../pom/cadDownloads';
import Login from '../pom/login';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const cadDownload = new CadDownload();

const {
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('12 CAD Download', () => {

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
    it('CAD Download - Login Menu UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        cadDownload.pageTitle().contains('History of CAD data download')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
    it('CAD Download - Login Menu DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        cadDownload.pageTitle().contains('CAD-Download Historie')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/    
    it('CAD Download - Login Menu IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        cadDownload.pageTitle().contains('Storico download dati CAD')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/    
    it('CAD Download - Login Menu FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        cadDownload.pageTitle().contains('Historique des téléchargements CAO')
    })


/*
    Description: eCatalog top page - Click the [My CAD data downloads] link on the right side of the page.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
    it('CAD Download - Side Menu UK', () => {
        cy.visit(url_uk)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        sideMenu.myCadDownloads().click()
        cadDownload.pageTitle().contains('History of CAD data download')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
    it('CAD Download - Side Menu DE', () => {
        cy.visit(url_de)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        sideMenu.myCadDownloads().click()
        cadDownload.pageTitle().contains('CAD-Download Historie')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/    
    it('CAD Download - Side Menu IT', () => {
        cy.visit(url_it)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        sideMenu.myCadDownloads().click()
        cadDownload.pageTitle().contains('Storico download dati CAD')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/    
    it('CAD Download - Side Menu FR', () => {
        cy.visit(url_fr)
        cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        cy.login()
        cy.wait("@initialLoad")

        sideMenu.myCadDownloads().click()
        cadDownload.pageTitle().contains('Historique des téléchargements CAO')
    })
});
