
const authUser = require('../../../fixtures/auth-user.json');
const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('14 Cart Test', () => {

/*
    Description: eCatalog top page - Click the [Cart] link on the right side of the page.
    Test Steps:
        1. Cart: the page is displayed. (Cart)
*/      
  it('My Components UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('.lc-cart').click()
    cy.contains('Products added to shopping cart')
  })

/*
    Description: eCatalog top page - Click the [Cart] link on the right side of the page.
    Test Steps:
        1. Cart: the page is displayed. (Cart)
*/      
  it('My Components DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('.lc-cart').click()
    cy.contains('Zu dem Einkaufswagen hinzugefügte Artikel ')
  })

/*
    Description: eCatalog top page - Click the [Cart] link on the right side of the page.
    Test Steps:
        1. Cart: the page is displayed. (Cart)
*/      
  it('My Components IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('.lc-cart').click()
    cy.contains('Prodotti aggiunti al carrello')
  })

/*
    Description: eCatalog top page - Click the [Cart] link on the right side of the page.
    Test Steps:
        1. Cart: the page is displayed. (Cart)
*/      
  it('My Components FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.get('.lc-cart').click()
    cy.contains('Produits ajoutés au panier')
  })
});