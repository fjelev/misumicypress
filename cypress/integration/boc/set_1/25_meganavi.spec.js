const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('25 Meganavi', () => {
/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Meganavi UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains('Automation Components')
    cy.contains('Fasteners')
    cy.contains('Materials')
    cy.contains('Wiring Components')
    cy.contains('› Electrical & Controls')
    cy.contains('Cutting Tools')
    cy.contains('Processing Tools')
    cy.contains('Material Handling & Storage')
    cy.contains('Safety & General Supplies')
    cy.contains('Lab & Clean Room Supplies')
    cy.contains('Press Die Components')
    cy.contains('Plastic Mold Components')
    cy.contains('Injection Molding Components')
    cy.contains('New Products')
    cy.contains('Recommended products')
     
    cy.contains('Original MISUMI Products')
    cy.contains('Brand Overview')
    cy.contains('Partner Brands')
    cy.contains('European Brands')
    cy.contains('Japanese Brands')
    cy.contains('Become a MISUMI Supplier')
    
     cy.get('[href*="/incadlibrary"]')
     cy.get('[href*="/applications/automotive-industry"]')
     cy.get('[href*="/applications/products-by-application/application-for-packaging-industry"]')
     cy.get('[href*="/applications/products-by-application/medical-and-pharma-industry"]')
     cy.get('[href*="/applications/products-by-application/3d-printer-parts"]')
     cy.get('[href*="/services/why-misumi"]')
     cy.get('[href*="/service/rd/icc/sw/pr/incadcomponents"]')
     cy.contains('Register')
     cy.contains('Contact')
     cy.contains('eProcurement')
     cy.contains('Newsletter')
     cy.get('[href*="/files/docs/supplier-self-declaration"]')
     cy.get('[href*="/services/faq-and-tutorials"]')
     cy.get('[href*="/services/catalog-request"]')
     cy.get('[href*="/services/eprocurement"]')
     cy.get('[href*="/services/newsletter"]');
     cy.contains('MISUMI Group')
     cy.contains('MISUMI Europe')
     cy.contains('Career')
     cy.contains('Responsibility')
     cy.contains('Customer References')
  })

/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Catalogue Request DE', () => {

  })


/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Catalogue Request IT', () => {
    
  })

/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
it('Catalogue Request FR', () => {
  
})

});






