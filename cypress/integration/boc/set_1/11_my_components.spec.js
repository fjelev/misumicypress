import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('11 My Components', () => {
  
/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
  it('My Components UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.wait(3000)
    cy.contains('My components').click()
    cy.wait(3000)
    cy.contains('Main folder')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
  it('My Components DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.wait(3000)
    cy.contains('Liste meiner Komponenten').click()
    cy.wait(3000)
    cy.contains('Hauptordner (Liste meiner Komponenten)')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
  it('My Components IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.wait(3000)
    cy.contains('Lista dei miei componenti').click()
    cy.wait(3000)
    cy.contains('Cartella principale (La lista dei miei componenti)')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
  it('My Components FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.wait(3000)
    cy.contains('Liste de mes composants').click()
    cy.wait(3000)
    cy.contains('Dossier principal (Liste de mes composants)')
  })

});