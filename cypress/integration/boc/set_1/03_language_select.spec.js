const authUser = require('../../../fixtures/auth-user.json')
const { url_uk, url_de, url_fr, url_it } = authUser

describe('03 Switch Language', () => {
	before(() => {
		cy.visit(url_uk)
		// log in only once before any of the tests run.
		// your app will likely set some sort of session cookie.
		// you'll need to know the name of the cookie(s), which you can find
		// in your Resources -> Cookies panel in the Chrome Dev Tools.
		cy.login()
	  })
	beforeEach(() => {
		// Cypress.Cookies.defaults({
		// 	preserve: ['cms_cookie','GACCESSTOKENKEY', 'VONA_COMMON_LOG_KEY']
		// })
	})
	/*
	Description: Switching between UK and DE local office and asserting that the correct language is displayed.
	Test Steps:
		1. Login onto the UK server with correct credentials and assert correct username
		2. Switch to German using the language select menu and assert German is displayed
	*/
	  it('My Page - Login Menu UK', () => {
	    cy.visit(url_uk)
	    cy.contains('Place an Order')
	    cy.changeLanguage('de')
	    cy.contains('Bestellen')
	  })

	/*
	Description: Switching between DE and IT local office and asserting that the correct language is displayed.
	Test Steps:
		1. Login onto the UK server with correct credentials and assert correct username
		2. Switch to German using the language select menu and assert German is displayed
	*/
	  it('My Page - Login Menu DE', () => {
	    cy.visit(url_de)
	    cy.contains('Bestellen')
	    cy.changeLanguage('it')
	    cy.contains('Ordina')
	  })

	/*
	Description: Switching between IT and FR local office and asserting that the correct language is displayed.
	Test Steps:
		1. Login onto the UK server with correct credentials and assert correct username
		2. Switch to German using the language select menu and assert German is displayed
	*/
	  it('My Page - Login Menu IT', () => {
	    cy.visit(url_it)
	    cy.contains('Ordina')
	    cy.changeLanguage('fr')
	    cy.contains('Commander')
	  })

	/*
	Description: Switching between FR and UK local office and asserting that the correct language is displayed.
	Test Steps:
		1. Login onto the UK server with correct credentials and assert correct username
		2. Switch to German using the language select menu and assert German is displayed
	*/
	  it('My Page - Login Menu FR', () => {
	    cy.visit(url_fr)
	    cy.contains('Commander')
	    cy.changeLanguage('uk')
	    cy.contains('Place an Order')
	  })
	});