const authUser = require('../../../fixtures/auth-user.json');
const {
  name,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('02 Login', () => {
  /*
    Description: Login onto the UK server using the provided credentials and assert
                 search bar is displayed on English.
    Test Steps:
        1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
        2. Assert logged with correct username is displayed
  */
  it('Login UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains(name)
  })


  /*
    Description: Login onto the DE server using the provided credentials and
                 assert logged with correct username is displayed
    Test Steps:
        1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
        2. Assert logged with correct username is displayed
  */
  it('Login DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains(name)
  })


  /*
    Description: Login onto the IT server using the provided credentials and
                 assert logged with correct username is displayed
    Test Steps:
        1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
        2. Assert logged with correct username is displayed
  */
  it('Login IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains(name)
  })


  /*
    Description: Login onto the FR server using the provided credentials and
                 assert logged with correct username is displayed
    Test Steps:
        1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
        2. Assert logged with correct username is displayed
  */
  it('Login FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.contains(name)
  })
});