const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('09 Order History', () => {

/*
    Description: eCatalog top page - Click the [Manage Order History] link on the right side of the page.
    Test Steps:
        1. Order history: the page is displayed.(MISUMI Home>My MISUMI>Order History)
*/
  it('Order History UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Manage Order History').click()
    cy.contains('Order History')
  })

/*
    Description: eCatalog top page - Click the [Manage Order History] link on the right side of the page.
    Test Steps:
        1. Order history: the page is displayed.(MISUMI Home>My MISUMI>Order History)
*/
  it('Order History DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Bestellhistorie').click()
    cy.contains('Bestellhistorie')
  })

/*
    Description: eCatalog top page - Click the [Manage Order History] link on the right side of the page.
    Test Steps:
        1. Order history: the page is displayed.(MISUMI Home>My MISUMI>Order History)
*/
  it('Order History IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Accedi a storico ordini').click()
    cy.contains('Storico ordini')
  })

/*
    Description: eCatalog top page - Click the [Manage Order History] link on the right side of the page.
    Test Steps:
        1. Order history: the page is displayed.(MISUMI Home>My MISUMI>Order History)
*/
  it('Order History FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Historique des commandes').click()
    cy.contains('Historique des commandes')
  })
});