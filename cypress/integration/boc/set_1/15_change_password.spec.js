import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const userMenu = new UserMenu();

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('15 Change Password', () => {

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Change Password')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Passwort ändern')
  })  

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Modifica password')
  })  

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Changer mon mot de passe')
  })



/*
    Description: eCatalog top page - Click the [Change password] link on the right side of the page.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Change password').invoke('removeAttr', 'target').click()
    cy.contains('Change Password')
  })

/*
    Description: eCatalog top page - Click the [Change password] link on the right side of the page.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Passwort ändern').invoke('removeAttr', 'target').click()
    cy.contains('Passwort ändern')
  })

/*
    Description: eCatalog top page - Click the [Change password] link on the right side of the page.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Modifica password').invoke('removeAttr', 'target').click()
    cy.contains('Modifica password')
  })

/*
    Description: eCatalog top page - Click the [Change password] link on the right side of the page.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.contains('Changer mon mot de passe').invoke('removeAttr', 'target').click()
    cy.contains('Changer mon mot de passe')
  })

});