import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  name,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('My Page - Login Menu UK', () => {

/*
  Description: eCatalog top page - Expand the menu of the [Login User Name] button in the upper right corner of the page and click the [My Page] link.
  Test Steps:
      1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/
  it('My Page - Login Menu UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    userMenu.userPanel().click()
    userMenu.myPageUserMenu().click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('My Page')
    myPage.pageTopic().contains('MISUMI Top Page')
  })

/*
  Description: eCatalog top page - Expand the menu of the [Login User Name] button in the upper right corner of the page and click the [My Page] link.
  Test Steps:
      1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/
  it('My Page - Login Menu DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    userMenu.userPanel().click()
    userMenu.myPageUserMenu().click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('Meine Seite')
    myPage.pageTopic().contains('MISUMI Startseite')
  })

/*
  Description: eCatalog top page - Expand the menu of the [Login User Name] button in the upper right corner of the page and click the [My Page] link.
  Test Steps:
      1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/
  it('My Page - Login Menu IT', () => {
    cy.visit(url_it)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    userMenu.userPanel().click()
    userMenu.myPageUserMenu().click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('La mia pagina')
    myPage.pageTopic().contains('Pagina principale MISUMI')
  })

/*
  Description: eCatalog top page - Expand the menu of the [Login User Name] button in the upper right corner of the page and click the [My Page] link.
  Test Steps:
      1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/  
  it('My Page - Login Menu FR', () => {
    cy.visit(url_fr)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    userMenu.userPanel().click()
    userMenu.myPageUserMenu().click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('Ma page')
    myPage.pageTopic().contains('Haut de la page MISUMI')
  })

/*
    Description: eCatalog top page - Click the [My Page] link on the right side of the page.
    Test Steps:
        1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/
  it('My Page - Sidemenu UK', () => {
    cy.visit(url_uk)
    cy.login()

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    sideMenu.myPageSideMenu().click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('My Page')
    myPage.pageTopic().contains('MISUMI Top Page')
  })

/*
    Description: eCatalog top page - Click the [My Page] link on the right side of the page.
    Test Steps:
        1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/
  it('My Page - Sidemenu DE', () => {
    cy.visit(url_de)
    cy.login()

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    sideMenu.myPageSideMenu().click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('Meine Seite')
    myPage.pageTopic().contains('MISUMI Startseite')
  })

/*
    Description: eCatalog top page - Click the [My Page] link on the right side of the page.
    Test Steps:
        1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/ 
  it('My Page - Sidemenu IT', () => {
    cy.visit(url_it)
    cy.login()

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    sideMenu.myPageSideMenu().click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('La mia pagina')
    myPage.pageTopic().contains('Pagina principale MISUMI')
  })

/*
    Description: eCatalog top page - Click the [My Page] link on the right side of the page.
    Test Steps:
        1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/ 
  it('My Page - Sidemenu FR', () => {
    cy.visit(url_fr)
    cy.login()

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    sideMenu.myPageSideMenu().click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('Ma page')
    myPage.pageTopic().contains('Haut de la page MISUMI')
  })

/*
    Description: eCatalog top page - Click the [Login User Name My Page] link on the right side of the page. Link with outlined character on the blue background
    Test Steps:
        1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/
  it('My Page Displayed - UK', () => {
    cy.visit(url_uk)
    cy.login()

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    cy.contains(`${name} My Page`).click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('My Page')
    myPage.pageTopic().contains('MISUMI Top Page')
  })

/*
    Description: eCatalog top page - Click the [Login User Name My Page] link on the right side of the page. Link with outlined character on the blue background
    Test Steps:
        1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/  
  it('My Page Displayed - DE', () => {
    cy.visit(url_de)
    cy.login()

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    cy.contains(`${name} Meine Seite`).click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('Meine Seite')
    myPage.pageTopic().contains('MISUMI Startseite')
  })

/*
    Description: eCatalog top page - Click the [Login User Name My Page] link on the right side of the page. Link with outlined character on the blue background
    Test Steps:
        1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/  
  it('My Page Displayed - IT', () => {
    cy.visit(url_it)
    cy.login()

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    cy.contains(`${name} Mia pagina`).click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('La mia pagina')
    myPage.pageTopic().contains('Pagina principale MISUMI')
  })

/*
    Description: eCatalog top page - Click the [Login User Name My Page] link on the right side of the page. Link with outlined character on the blue background
    Test Steps:
        1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
*/  
  it('My Page Displayed - FR', () => {
    cy.visit(url_fr)
    cy.login()

    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    cy.contains(`${name} Ma page`).click()

    cy.wait(`@waitForMyPage`)
    myPage.pageTitle().contains('Ma page')
    myPage.pageTopic().contains('Haut de la page MISUMI')
  })

});
