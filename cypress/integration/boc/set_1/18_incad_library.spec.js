import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('18 Incad Library', () => {

/*
  Description: On eCatalog top page, click the [inCAD Library] link at the bottom of eCatalog.
  Test Steps:
      1. inCAD Library is displayed. (https://pre-uk.misumi-ec.com/eu/incadlibrary/)
*/ 
  it('Incad Library UK', () => {
    cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('.inCAD-library > .rapid-image > a').invoke('removeAttr', 'target').click()
    cy.get('.closebutton').click()
    cy.get('[href="/eu/incadlibrary/detail/000153.html"]')
    cy.url().should('include', 'uk.misumi-ec.com/eu/incadlibrary')
  })

/*
  Description: On eCatalog top page, click the [inCAD Library] link at the bottom of eCatalog.
  Test Steps:
      1. inCAD Library is displayed. (https://pre-de.misumi-ec.com/eu/incadlibrary/)
*/ 
  it('Incad Library DE', () => {
    cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.login()
    cy.wait("@initialLoad")
    cy.get('.inCAD-library > .rapid-image > a').invoke('removeAttr', 'target').click()
    cy.get('.closebutton').click()
    cy.get('[href="/eu/incadlibrary/detail/000153.html"]')
    cy.url().should('include', 'de.misumi-ec.com/eu/incadlibrary')
  })

});