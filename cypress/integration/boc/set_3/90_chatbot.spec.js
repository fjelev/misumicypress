import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('90 Chatbot ', () => {

  /*
    Description: 
    Test Steps:
        1. 

  */
  it('Chatbot UK', () => {
    cy.visit(url_uk)
      cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
      cy.login()
      cy.wait("@initialLoad")    

    cy.get('#solvemate-widget-button').then($iframe => {
      const $body = $iframe.contents().find('body')
      cy.wrap($body)
        .find('#widget-button')
        .click()
        .wait(4000)
    })

    cy.get('#solvemate-widget').then($iframe => {
      const $body = $iframe.contents().find('body')
      cy.wrap($body)
        .contains('Digital Assistant')
    })

  })

  it('Chatbot IT', () => {
    cy.visit(url_it)
      cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
      cy.login()
      cy.wait("@initialLoad")    

    cy.get('#solvemate-widget-button').then($iframe => {
      const $body = $iframe.contents().find('body')
      cy.wrap($body)
        .find('#widget-button')
        .click()
        .wait(4000)
    })

    cy.get('#solvemate-widget').then($iframe => {
      const $body = $iframe.contents().find('body')
      cy.wrap($body)
        .contains('Expert')
    })

  })

  it('Chatbot DE', () => {
    cy.visit(url_de)
      cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
      cy.login()
      cy.wait("@initialLoad")    

    cy.get('#solvemate-widget-button').then($iframe => {
      const $body = $iframe.contents().find('body')
      cy.wrap($body)
        .find('#widget-button')
        .click()
        .wait(4000)
    })

    cy.get('#solvemate-widget').then($iframe => {
      const $body = $iframe.contents().find('body')
      cy.wrap($body)
        .contains('Expert')
    })

  })

  it('Chatbot FR', () => {
    cy.visit(url_fr)
      cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
      cy.login()
      cy.wait("@initialLoad")    

    cy.get('#solvemate-widget-button').then($iframe => {
      const $body = $iframe.contents().find('body')
      cy.wrap($body)
        .find('#widget-button')
        .click()
        .wait(4000)
    })

    cy.get('#solvemate-widget').then($iframe => {
      const $body = $iframe.contents().find('body')
      cy.wrap($body)
        .contains('Expert')
    })

  })

})