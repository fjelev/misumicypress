import Login from '../pom/login'
import MyPage from '../pom/myPage'
import SideMenu from '../pom/sideMenu'
import UserMenu from '../pom/userMenu'

const authUser = require('../../../fixtures/auth-user.json')
const login = new Login()
const userMenu = new UserMenu()
const sideMenu = new SideMenu()
const myPage = new MyPage()
const { username, password, url_uk, url_de, url_fr, url_it } = authUser
const LRBS110_part =
	'/vona2/detail/110200080340/?PNSearch=LRBS110-15&HissuCode=LRBS110-15&searchFlow=suggest2products&Keyword=LRBS110-15'
describe('93 Cookie Banner ', () => {

    /*
      Description: 
      Test Steps:
          1.
  
    */

	// ACCEPT
	it('Accpet Cookie Banner UK', () => {
		let assertCookie
		cy.visit(url_uk)
    cy.clearCookies()
		cy.getCookies().then(cookie => {
			assert.isAtMost(cookie.length, 1)
		})
		login.acceptCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Accpet Cookie Banner IT', () => {
		let assertCookie
		cy.visit(url_it)
    cy.clearCookies()
		cy.getCookies().then(cookie => {
			assert.isAtMost(cookie.length, 1)
		})
		login.acceptCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Accpet Cookie Banner DE', () => {
		let assertCookie
		cy.visit(url_de)
    cy.clearCookies()
		cy.getCookies().then(cookie => {
			assert.isAtMost(cookie.length, 1)
		})
		login.acceptCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Accpet Cookie Banner FR', () => {
		let assertCookie
		cy.visit(url_fr)
    cy.clearCookies()
		cy.getCookies().then(cookie => {
			assert.isAtMost(cookie.length, 1)
		})
		login.acceptCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	// Not Accepting case - apparently not needed
	
	// ESSENTIAL
	it('Essential Cookie Banner UK', () => {
		let assertCookie
		cy.visit(url_uk)
    cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		login.essentialCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Essential Cookie Banner IT', () => {
		let assertCookie
		cy.visit(url_it)
    cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		login.essentialCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Essential Cookie Banner DE', () => {
		let assertCookie
		cy.visit(url_de)
    cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		login.essentialCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Essential Cookie Banner FR', () => {
		let assertCookie
		cy.visit(url_fr)
    cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		login.essentialCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	// DIRECT LINK

	it('Direct Link Not Cookie Banner UK', () => {
    let assertCookie
	cy.visit(url_uk + LRBS110_part)
    cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		expect(login.essentialCookies()).to.exist
    cy.getCookies().then(cookie => {
			cy.log(assertCookie)
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"recentryCategoryView"') {
					assertCookie = JSON.stringify(cookie.name)
					expect(assertCookie).eql('"recentryCategoryView"')
				}
				assertCookie = JSON.stringify(cookie.name)
				cy.log(assertCookie)
			})
    	})
	})

	it('Direct Link Not Cookie Banner IT', () => {
		let assertCookie
		cy.visit(url_it + LRBS110_part)
		cy.clearCookies()
			cy.getCookies().then(cookie => {
				cy.log(JSON.stringify(cookie))
				assert.isAtMost(cookie.length, 1)
			})
			expect(login.essentialCookies()).to.exist
		cy.getCookies().then(cookie => {
			cy.log(assertCookie)
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"recentryCategoryView"') {
					assertCookie = JSON.stringify(cookie.name)
					expect(assertCookie).eql('"recentryCategoryView"')
				}
				assertCookie = JSON.stringify(cookie.name)
				cy.log(assertCookie)
			})
		})
	})

	it('Direct Link Not Cookie Banner DE', () => {
		let assertCookie
		cy.visit(url_de + LRBS110_part)
		cy.clearCookies()
			cy.getCookies().then(cookie => {
				cy.log(JSON.stringify(cookie))
				assert.isAtMost(cookie.length, 1)
			})
			expect(login.essentialCookies()).to.exist
		cy.getCookies().then(cookie => {
			cy.log(assertCookie)
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"recentryCategoryView"') {
					assertCookie = JSON.stringify(cookie.name)
					expect(assertCookie).eql('"recentryCategoryView"')
				}
				assertCookie = JSON.stringify(cookie.name)
				cy.log(assertCookie)
			})
		})
	})

	it('Direct Link Not Cookie Banner FR', () => {
		let assertCookie
		cy.visit(url_fr + LRBS110_part)
		cy.clearCookies()
			cy.getCookies().then(cookie => {
				cy.log(JSON.stringify(cookie))
				assert.isAtMost(cookie.length, 1)
			})
			expect(login.essentialCookies()).to.exist
		cy.getCookies().then(cookie => {
			cy.log(assertCookie)
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"recentryCategoryView"') {
					assertCookie = JSON.stringify(cookie.name)
					expect(assertCookie).eql('"recentryCategoryView"')
				}
				assertCookie = JSON.stringify(cookie.name)
				cy.log(assertCookie)
			})
		})
	})
})
