import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('91 CAD data ', () => {

  /*
      Description: eCatalog top page - expand the menu of [login user name] on the top right of the page and click [Logout] link.
      Test Steps:
          1. Logout process is executed, and the notation of [login user name] button changes to Login.
  
    */
  it('UK SFJ10-100 - normal CADENAS flow', () => {
    cy.visit(url_uk)

  })

  // it('UK LRBS110', () => {
  //   cy.visit(url_de)
  //   cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  //   cy.login()
  //   cy.wait("@initialLoad")

  //   cy.searchPart('SFJ3-100')
  //   cy.contains('SFJ3-100')

  // })

  // it('UK PSTS10A-30 - normal CADENAS flow', () => {
  //   cy.visit(url_it)
  //   cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  //   cy.login()
  //   cy.wait("@initialLoad")
  // })

  // it('UK RBNT5 - normal CADENAS flow', () => {
  //   cy.visit(url_fr)
  //   cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  //   cy.login()
  //   cy.wait("@initialLoad")
  // })

  // it('UK HCCG4-10 - normal CADENAS flow', () => {
  //   cy.visit(url_fr)
  //   cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  //   cy.login()
  //   cy.wait("@initialLoad")
  // })

  // it('Add to cart SINUS CAD', () => {
  //   cy.visit(url_fr)
  //   cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  //   cy.login()
  //   cy.wait("@initialLoad")
  // })

  // it('3D Preview Part Number check', () => {
  //   cy.visit(url_fr)
  //   cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  //   cy.login()
  //   cy.wait("@initialLoad")
  // })

  // it('UK HD-B8-20-TC2 Error 500', () => {
  //   cy.visit(url_fr)
  //   cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  //   cy.login()
  //   cy.wait("@initialLoad")
  // })

  // it('UK MBJ22-60 Error 500', () => {
  //   cy.visit(url_fr)
  //   cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  //   cy.login()
  //   cy.wait("@initialLoad")
  // })

  // it('UK M-TCN Error 404', () => {
  //   cy.visit(url_fr)
  //   cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
  //   cy.login()
  //   cy.wait("@initialLoad")
  // })
});

// test.page(environment.url_de).httpAuth({
//   username: environment.basic_auth_user,
//   password: environment.basic_auth_password,
// })("Add to cart SINUS CAD", async () => {
//   // await login(t)
//   // await cartPage.deleteCart(t);
//   // await cartPage.addCartItem(t, "LRBS110-115", 1)
// });

// test.page(environment.url_de)("3D Preview Part Number check", async () => {});

// test.page(environment.url_de)("UK HD-B8-20-TC2 Error 500", async () => {});

// test.page(environment.url_de)("UK MBJ22-60 Error 500", async () => {});

// test.page(environment.url_de)("UK M-TCN Error 404", async () => {});

// test.page(environment.url_it)(
//   "UK SFJ10-100 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(SFJ10_100_part, SFJ10_100_part_screenshot)
//   }
// );

// test.page(environment.url_it)("UK LRBS110", async () => {
//   // await checkSinusCAD(LRBS110_part, LRBS110_part_screenshot)
// });

// test.page(environment.url_it)(
//   "UK PSTS10A-30 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(PSTS10A_30_part, PSTS10A_30_part_screenshot)
//   }
// );

// test.page(environment.url_it)("UK RBNT5 - normal CADENAS flow", async () => {
//   // await checkSinusCAD(RBNT5_part, RBNT5_part_screenshot)
// });

// test.page(environment.url_it)(
//   "UK HCCG4-10 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(HCCG4_10_part, HCCG4_10_part_screenshot)
//   }
// );

// test.page(environment.url_it).httpAuth({
//   username: environment.basic_auth_user,
//   password: environment.basic_auth_password,
// })("Add to cart SINUS CAD", async () => {
//   // await login(t)
//   // await cartPage.deleteCart(t);
//   // await cartPage.addCartItem(t, "LRBS110-115", 1)
// });

// test.page(environment.url_it)("3D Preview Part Number check", async () => {});

// test.page(environment.url_it)("UK HD-B8-20-TC2 Error 500", async () => {});

// test.page(environment.url_it)("UK MBJ22-60 Error 500", async () => {});

// test.page(environment.url_it)("UK M-TCN Error 404", async () => {});

// test.page(environment.url_fr)(
//   "UK SFJ10-100 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(SFJ10_100_part, SFJ10_100_part_screenshot)
//   }
// );

// test.page(environment.url_fr)("UK LRBS110", async () => {
//   // await checkSinusCAD(LRBS110_part, LRBS110_part_screenshot)
// });

// test.page(environment.url_fr)(
//   "UK PSTS10A-30 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(PSTS10A_30_part, PSTS10A_30_part_screenshot)
//   }
// );

// test.page(environment.url_fr)("UK RBNT5 - normal CADENAS flow", async () => {
//   // await checkSinusCAD(RBNT5_part, RBNT5_part_screenshot)
// });

// test.page(environment.url_fr)(
//   "UK HCCG4-10 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(HCCG4_10_part, HCCG4_10_part_screenshot)
//   }
// );

// // test.page(environment.url_fr).httpAuth({
// //   username: environment.basic_auth_user,
// //   password: environment.basic_auth_password,
// // })("Add to cart SINUS CAD", async () => {
//   // await login(t)
//   // await cartPage.deleteCart(t);
//   // await cartPage.addCartItem(t, "LRBS110-115", 1)
// });

// test.page(environment.url_fr)("3D Preview Part Number check", async () => {});

// test.page(environment.url_fr)("UK HD-B8-20-TC2 Error 500", async () => {});

// test.page(environment.url_fr)("UK MBJ22-60 Error 500", async () => {});

// test.page(environment.url_fr)("UK M-TCN Error 404", async () => {});

// SFJ10-100 - normal CADENAS flow
// PSTS10A-30 - normal CADENAS flow
// RBNT5 - normal CADENAS flow
// HCCG4-10 - normal CADENAS flow

// SINUS CAD part numbers

// LRBS110-15 - normal functionality flow
// HD-B8-20-TC2 - error 500
// MBJ22-60 - error 500
// M-TCN - error 404
