import Login from '../pom/login';
import MyPage from '../pom/myPage';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const myPage = new MyPage();
const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('My Page - Login Menu UK', () => {
  it('My Page - Login Menu UK', () => {
    cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    login.acceptCookies().click()
    login.ociSignIn()
    cy.visit(url_uk)
    userMenu.userPanel().click()
    userMenu.myPageUserMenu().click()

    myPage.pageTitle().contains('My Page')
    myPage.pageTopic().contains('MISUMI Top Page')
  })

  it('My Page - Login Menu DE', () => {
    cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    login.acceptCookies().click()
    login.ociSignIn()
    cy.visit(url_de)
    userMenu.userPanel().click()
    userMenu.myPageUserMenu().click()

    myPage.pageTitle().contains('Meine Seite')
    myPage.pageTopic().contains('MISUMI Startseite')
  })

  it('My Page - Login Menu IT', () => {
    cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    login.acceptCookies().click()
    login.ociSignIn()
    cy.visit(url_it)
    userMenu.userPanel().click()
    userMenu.myPageUserMenu().click()

    myPage.pageTitle().contains('La mia pagina')
    myPage.pageTopic().contains('Pagina principale MISUMI')
  })

  it('My Page - Login Menu FR', () => {
    cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    login.acceptCookies().click()
    login.ociSignIn()
    cy.visit(url_fr)
    userMenu.userPanel().click()
    userMenu.myPageUserMenu().click()

    myPage.pageTitle().contains('Ma page')
    myPage.pageTopic().contains('Haut de la page MISUMI')
  })

  // it('My Page - Sidemenu UK', () => {
  //   cy.visit(url_uk)
  //   login.signIn()
  //
  //   sideMenu.myPageSideMenu().click()
  //
  //   myPage.pageTitle().contains('My Page')
  //   myPage.pageTopic().contains('MISUMI Top Page')
  // })
  //
  // it('My Page - Sidemenu DE', () => {
  //   cy.visit(url_de)
  //   login.signIn()
  //
  //   sideMenu.myPageSideMenu().click()
  //
  //   myPage.pageTitle().contains('Meine Seite')
  //   myPage.pageTopic().contains('MISUMI Startseite')
  // })
  //
  // it('My Page - Sidemenu IT', () => {
  //   cy.visit(url_it)
  //   login.signIn()
  //
  //   sideMenu.myPageSideMenu().click()
  //
  //   myPage.pageTitle().contains('La mia pagina')
  //   myPage.pageTopic().contains('Pagina principale MISUMI')
  // })
  //
  // it('My Page - Sidemenu FR', () => {
  //   cy.visit(url_fr)
  //   login.signIn()
  //
  //   sideMenu.myPageSideMenu().click()
  //
  //   myPage.pageTitle().contains('Ma page')
  //   myPage.pageTopic().contains('Haut de la page MISUMI')
  // })

});
