const authUser = require('../../../fixtures/auth-user.json');

class Login {
    ociSignIn() {
        const eCatalog = cy.get('[class="btnOpenEcatalog"]');
        eCatalog.click()
        return this
    }

    acceptCookies() {
        return cy.get('.l-cookieNotification__accept');
    }
}

export default Login;
