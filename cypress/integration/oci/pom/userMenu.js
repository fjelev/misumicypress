class UserMenu {
    userPanel() {
        return cy.get('[data-header-func="user"]');
    }

    myPageUserMenu() {
        return cy.get('.l-header__usermenu [data-common-userlink="mypagetop"]')
    }

    myPageSideMenu() {
        return cy.get('.l-header__usermenu [data-common-userlink="mypagetop"]')
    }

    myCadDownloads() {
        return cy.get('.l-header__usermenu [data-common-userlink="cadhistory"]')
    }

    myComponents() {
        return cy.get('.l-header__usermenu [data-common-userlink="partslist"]')
    }

    changeUserData() {
        return cy.get('.l-header__usermenu [data-common-userlink="editprofile"]')
    }

    changePassword() {
        return cy.get('.l-header__usermenu [data-common-userlink="passchange"]')
    }


}

export default UserMenu;