FROM cypress/base:10
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN npm install
RUN mkdir /.npm
RUN chmod +x entrypoint.sh
RUN ls -l
RUN pwd
RUN $(npm bin)/cypress verify
#ARG ARG_SCRIPT="Default_Value"
#RUN echo "ARG_SCRIPT is ${ARG_SCRIPT}"
ENTRYPOINT ["/app/entrypoint.sh"]
#ENTRYPOINT npm run ${ARG_SCRIPT}
#RUN ["npm", "run", "cy:t"]
